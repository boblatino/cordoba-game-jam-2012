#!/usr/bin
g++ -Wall -ansi -O3 -m32 -I. -I/usr/local/include/bullet -I/home/agustin/programacion/gamejam/gamejam2013/irrlicht-1.7.3/include -c ./collisionobjectaffectorattract.cpp ./convexhullshape.cpp ./collisionobject.cpp ./bulletworld.cpp ./liquidbody.cpp ./gimpactmeshshape.cpp ./collisionobjectaffector.cpp ./raycastvehicle.cpp ./rigidbody.cpp ./boxshape.cpp ./kinematiccharactercontroller.cpp ./collisionshape.cpp ./sphereshape.cpp ./trianglemeshshape.cpp ./softbody.cpp ./physicsdebug.cpp ./collisionobjectaffectordelete.cpp ./motionstate.cpp ./irrbulletcommon.cpp ./collisioncallbackinformation.cpp ./bvhtrianglemeshshape.cpp ./irrbullet.cpp -L/usr/local/lib -L/home/agustin/programacion/gamejam/gamejam2013/irrlicht-1.7.3/lib/Linux -lIrrlicht -lGL -lXxf86vm -lXext -lX11 -lBulletSoftBody -lBulletDynamics -lBulletCollision -lLinearMath

#-shared -fPIC -o libirrBullet.so


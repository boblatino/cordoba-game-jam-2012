#pragma once

#include <vector3d.h>

class ICollisionObjectAffector;
class irrBulletWorld;
class IRigidBody;

#include <vector>

class BulletGeneratorSystem
{
public:
    enum class BulletType
    {
        RedGlobule,
        WhiteGlobule
    };

    struct BulletProperties
    {
        BulletProperties(const BulletType bulletType = BulletType::RedGlobule, const irr::core::vector3df initialPosition = irr::core::vector3df(0.0f, 0.0f, 0.0f),
            const irr::core::vector3df scale = irr::core::vector3df(1.0f, 1.0f, 1.0f), const irr::core::vector3df linearVelocity = irr::core::vector3df(0.0f, 0.0f, 0.0f), 
            const float mass = 0.0f, const float friction = 0.0f)
            : mBulletType(bulletType)
            , mInitialPosition(initialPosition)
            , mLinearVelocity(linearVelocity)
            , mScale(scale)
            , mMass(mass)
            , mFriction(friction)
        {

        }

        BulletType mBulletType; 
        irr::core::vector3df mInitialPosition;
        irr::core::vector3df mLinearVelocity;
        irr::core::vector3df mScale;
        float mMass;
        float mFriction;
        std::vector<ICollisionObjectAffector*> mAffectors;
    };

    IRigidBody* generateBullet(const BulletProperties& properties, irrBulletWorld& world);
};

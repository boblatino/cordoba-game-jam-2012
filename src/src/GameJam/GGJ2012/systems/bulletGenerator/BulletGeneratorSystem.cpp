#include "BulletGeneratorSystem.h"

#include <ISceneManager.h>
#include <IMeshSceneNode.h>

#include <engine/IrrlichtGlobals.h>

#include <irrbullet.h>

IRigidBody* BulletGeneratorSystem::generateBullet(const BulletProperties& properties,
                                                  irrBulletWorld& world)
{
    // Create sphere's scene node
    irr::scene::IMeshSceneNode* node = IrrlichtGlobals::sceneManager().addSphereSceneNode();
    node->setScale(properties.mScale);
    node->setPosition(properties.mInitialPosition);
    node->setMaterialFlag(irr::video::EMF_LIGHTING, true);
    node->setMaterialFlag(irr::video::EMF_NORMALIZE_NORMALS, true);

    switch (properties.mBulletType)
    {        
    case BulletType::RedGlobule:
        node->setMaterialTexture(0, IrrlichtGlobals::device().getVideoDriver()->getTexture("media/images/venas_03.jpg"));
        break;

    case BulletType::WhiteGlobule:
        node->setMaterialTexture(0, IrrlichtGlobals::device().getVideoDriver()->getTexture("media/images/venas_03.jpg"));
        break;

    default:
        break;
    }

    // Create rigid body according cube scene node.
    ICollisionShape *shape = new ISphereShape(node, properties.mMass, true);
    IRigidBody *body = world.addRigidBody(shape);
    
    // Set rigid body properties
    body->setLinearVelocity(properties.mLinearVelocity);
    body->setDamping(0.2f, 0.2f);
    body->setFriction(properties.mFriction);
    body->getAttributes()->addBool("collide", true);

    // Add bullet affectors
    for (size_t i = 0, size = properties.mAffectors.size(); i < size; ++i){
        body->addAffector(properties.mAffectors[i]);
    }

    return body;
}

/*
 * Enemy.h
 *
 *  Created on: Jan 26, 2013
 *      Author: agustin
 */

#ifndef ENEMY_H_
#define ENEMY_H_

#include <string>

#include <ISceneNode.h>
#include <vector2d.h>
#include <vector3d.h>

#include <utils/DebugUtil.h>

class ICollisionObjectAffector;


class Enemy {
public:
    struct Circumference {
        irr::core::vector2df center;
        float radius;
    };

    static const int MAX_ROT_DEGREE = 15;
    static const float TOLERANCE;
public:
    Enemy(const std::string &meshPath);
    virtual ~Enemy();

    /**
     * Configure the enemy and returns the affector
     */
    ICollisionObjectAffector* configure(const float vel,
                                        const float force,
                                        const float maxY = 100.0f,
                                        const float minY = 25.0f);

    /**
     * set the two circunferences where the enemy could move
     */
    inline void setMovementZone(const Circumference &min, const Circumference &max);

    // set the position
    inline void setPos(const irr::core::vector3df &pos);

    // update the enemy positions
    void update(void);

private:
    Enemy(const Enemy &);
    Enemy &operator=(const Enemy&);

    // Get the next point to move
    void getNextPoint(irr::core::vector3df &point);

    // Check if we should need to continue moving to the point where we want to
    // go
    inline bool continueMoving(void);

private:
    irr::scene::ISceneNode *mNode;
    float mVelocity;
    float mMaxY;
    float mMinY;
    Circumference mMin;
    Circumference mMax;
    int mActualDegree;
    irr::core::vector3df mNextPoint;
    irr::core::vector3df mMoveVector;
    irr::core::vector3df mRotVector;
};


inline void
Enemy::setMovementZone(const Circumference &min, const Circumference &max)
{
    ASSERT(max.center == min.center && min.center.X == 0 && min.center.Y == 0);
    mMin = min;
    mMax = max;
    getNextPoint(mNextPoint);
}

inline void
Enemy::setPos(const irr::core::vector3df &pos)
{
    mNode->setPosition(pos);
    getNextPoint(mNextPoint);
}

inline bool
Enemy::continueMoving(void)
{
    return mNextPoint.getDistanceFromSQ(mNode->getPosition()) > TOLERANCE;
}


#endif /* ENEMY_H_ */

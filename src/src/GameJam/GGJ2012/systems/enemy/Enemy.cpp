/*
 * Enemy.cpp
 *
 *  Created on: Jan 26, 2013
 *      Author: agustin
 */

#include "Enemy.h"

#include <cmath>

#include <IAnimatedMesh.h>
#include <ISceneManager.h>
#include <ISceneNode.h>
#include <IAnimatedMeshSceneNode.h>

#include <collisionobjectaffectorattract.h>

#include <engine/IrrlichtGlobals.h>
#include <utils/DebugUtil.h>



#define CONVERT_D_TO_R(x) ((3.14159265f/180.0f) * (x))

const float Enemy::TOLERANCE = 100.0f;


void
Enemy::getNextPoint(irr::core::vector3df &point)
{
    // get the next degree rotation
    mActualDegree = (mActualDegree + MAX_ROT_DEGREE) % 360;
    const float diff = mMax.radius - mMin.radius;
    ASSERT(diff >= 0.0f);
    const float rnd = std::rand() % static_cast<int>(diff);
    const float module = mMin.radius + rnd;
    const int yVal = (std::rand() % static_cast<int>(mMaxY - mMinY)) + mMinY;

    point.X = std::cos(CONVERT_D_TO_R(mActualDegree));
    point.Y = 0;
    point.Z = std::sin(CONVERT_D_TO_R(mActualDegree));
    point.normalize();
    point *= module;
    point.Y = yVal;
    mMoveVector = point - mNode->getPosition();
    mMoveVector.normalize();
    mMoveVector *= mVelocity;
}



Enemy::Enemy(const std::string &meshPath) :
mNode(0),
mVelocity(1.0f),
mMaxY(400.0f),
mMinY(100.0f),
mActualDegree(0)
{
    // construct the scene nodes
   irr::scene::ISceneManager& sceneManager = IrrlichtGlobals::sceneManager();
   irr::scene::IAnimatedMesh *enemy = sceneManager.getMesh(meshPath.c_str());
   ASSERT(enemy);
   mNode = sceneManager.addAnimatedMeshSceneNode(enemy);
   ASSERT(mNode);

   mRotVector.X = 0.1f;
   mRotVector.Y = 0.2f;
   mRotVector.Z = 0.3f;

}

Enemy::~Enemy()
{

}


ICollisionObjectAffector*
Enemy::configure(const float vel,
                 const float force,
                 const float maxY,
                 const float minY)
{
    ICollisionObjectAffector* affector = new ICollisionObjectAffectorAttract(
                        mNode, force);
    mVelocity = vel;
    mMaxY = maxY;
    mMinY = minY;

    return affector;
}

// update the enemy positions
void
Enemy::update(void)
{
    mNode->setRotation(mNode->getRotation() + mRotVector);
    if (continueMoving()) {
        // translate the objective
        mNode->setPosition(mNode->getPosition() + mMoveVector *
                IrrlichtGlobals::lastTimeFrame());
        return;
    } else {
        getNextPoint(mNextPoint);
    }
    // translate the objective
    mNode->setPosition(mNode->getPosition() + mMoveVector *
            IrrlichtGlobals::lastTimeFrame());

}

#include "SceneLoader.h"

#include <ISceneManager.h>

#include <engine/IrrlichtGlobals.h>

#include <cassert>
#include <cstdint>
#include <sstream>

#include <irrbullet.h>

namespace
{
    float massFactors[10];

    void initMassFactors() 
    {
        const float increment = 0.4f;
        const float minMass = 0.1f;
        for (uint32_t i = 0; i < 10; ++i)
        {
            massFactors[i] = (i * increment) + minMass;
        }
    }

    float calculateMass(const uint32_t massIndex)
    {
        assert(massIndex < 10);
        
        float mass = 0.0f;

        if (massIndex == 0) {
            mass = 0.01f;
        } else if (massIndex == 9) {
            mass = 20.0f;
        } else {
            mass = massFactors[massIndex];
        }

        return mass;
    }

    struct ShapeProperties
    {
        ShapeProperties(ICollisionShape* collisionShape = nullptr, const bool collides = true, const bool isTarget = false)
            : mCollisionShape(collisionShape)
            , mCollides(collides)
            , mIsTarget(isTarget)
        {

        }

        ICollisionShape* mCollisionShape;
        bool mCollides;
        bool mIsTarget;
    };

    ShapeProperties getShape(irr::scene::ISceneNode& sceneNode, const uint32_t massIndex, std::string& shapeName)
    {
        ShapeProperties shapeProperties;

        if (shapeName == "cub" || shapeName == "cyl") 
            shapeProperties.mCollisionShape = new IBoxShape(&sceneNode, calculateMass(massIndex), false);   
        
        else if (shapeName == "sph") 
            shapeProperties.mCollisionShape = new ISphereShape(&sceneNode, calculateMass(massIndex), false);
         
        else if (shapeName == "sta") 
        {
            shapeProperties.mCollisionShape = new IBoxShape(&sceneNode, 0.0f, false);

            // We do not consider collide property with static meshes.
            shapeProperties.mCollides = false;
        } else if (shapeName == "tar") {
            shapeProperties.mCollisionShape = new ISphereShape(&sceneNode, 0.0f, false);
            shapeProperties.mIsTarget = true;
        }

        return shapeProperties;
    }
}

void SceneLoader::loadScene(irrBulletWorld& world, const std::string& fileName)
{
    initMassFactors();

    // Load irr scene generated with CopperCube
    IrrlichtGlobals::sceneManager().loadScene(fileName.c_str());

    // Extract all scene nodes loaded into Scene Manager.
    irr::core::array<irr::scene::ISceneNode *> nodes;
    IrrlichtGlobals::sceneManager().getSceneNodesFromType(irr::scene::ESNT_ANY, nodes); 

    // Iterate through each node and load its corresponding body 
    // into bullet world
    std::string nodeName;
    std::string shapeName;
    std::stringstream ss; 
    const uint32_t size = nodes.size();
    for (uint32_t i = 0; i < nodes.size(); ++i)
    {
        irr::scene::ISceneNode* node = nodes[i];
                
        IRigidBody *body = nullptr;

        // Get node name
        nodeName = node->getName();

        if (nodeName.empty()) 
            continue;

        // Check the first 3 characters
        // which defines the element's type.
        // tar = target
        // cub = cube
        // cyl = cylinder
        // sph = sphere
        // sky = skybox
        // sta = static 
        // Also check the following characters
        // that have different meanings.
        // [3] = mass factor        
        shapeName = nodeName.substr(0, 3);        
        std::string str = nodeName.substr(3, 1);
        ss << str;
        uint32_t massIndex;
        ss >> massIndex;        

        // Reset string stream
        ss.str("");
        ss.clear();
        ShapeProperties shapeProperties = getShape(*node, massIndex, shapeName);
        
        // If shape is null, then we should not create any 
        // physics body for that node.
        if (shapeProperties.mCollisionShape) {
            // Create rigid body and set its properties
            IRigidBody *body = world.addRigidBody(shapeProperties.mCollisionShape);
            body->setDamping(0.2f, 0.2f);
            body->setFriction(1.0f);
            body->getAttributes()->addBool("collide", shapeProperties.mCollides);
            body->getAttributes()->addBool("isTarget", shapeProperties.mIsTarget);           
        }
    }
}
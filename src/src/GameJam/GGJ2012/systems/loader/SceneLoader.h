#pragma once

#include <string>

class irrBulletWorld;

class SceneLoader
{
public:
    void loadScene(irrBulletWorld& world, const std::string& fileName);
};
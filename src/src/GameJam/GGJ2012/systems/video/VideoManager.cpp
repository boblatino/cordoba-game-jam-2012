#include "VideoManager.h"
#include <engine/IrrlichtGlobals.h>

VideoManager::VideoManager() : mVidPlayer (&IrrlichtGlobals::videoDriver(), &IrrlichtGlobals::timer(), 1024, 768) {
}

void VideoManager::play(std::string name) {
	mVidPlayer.open(name.c_str());
}

bool VideoManager::update() {
	if (!mVidPlayer.refresh()) {  
        return false;            
    } else {  
        mVidPlayer.drawVideoTexture();
		return true;
    }  
}
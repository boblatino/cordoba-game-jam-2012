#pragma once
#include <string>
#include <systems/video/video.h>

class VideoManager {
	public:
		VideoManager(); 
		void play(std::string name);
		bool update();

private:
	murmuurVIDEO mVidPlayer;

};
#pragma once

#include <IBillboardSceneNode.h>
#include <IMeshManipulator.h>
#include <IMeshSceneNode.h>
#include <IParticleSystemSceneNode.h>
#include <IrrlichtDevice.h>
#include <ISceneManager.h>
#include <ISceneNode.h>
#include <IAnimatedMesh.h>

#include <IParticleSystemSceneNode.h>
#include <map>
#include <cstdlib>
#include <vector>
#include <engine/IrrlichtGlobals.h>

#define TIME_TO_DISMISS_GLOWINGS 5.0f
#define TIME_TO_DISMISS_FIRE .3f

class SpecialEffects {
    class Globule {
    public:
        Globule(irr::scene::ISceneNode *n) :
            mNode(n)
        {
            mDirVec.X = (std::rand() % 100) * 0.0005f;
            mDirVec.Y = (std::rand() % 100) * 0.0005f;
            mDirVec.Z = (std::rand() % 100) * 0.0005f;
            mRot.X = (std::rand() % 100) * 0.001f;
            mRot.Y = (std::rand() % 100) * 0.001f;
            mRot.Z = (std::rand() % 100) * 0.001f;
        }

        void update();

        static irr::core::vector3df mMax;
        static irr::core::vector3df mMin;
    private:
        irr::scene::ISceneNode *mNode;
        irr::core::vector3df mDirVec;
        irr::core::vector3df mRot;

    };
public:
	SpecialEffects(irr::IrrlichtDevice& device);
	~SpecialEffects();
    void createGlowing(const irr::core::vector3df &pos);
	void createFire(const irr::core::vector3df &pos);


	// configure the box where the globules will move
	void createGlobules(const int num, const irr::core::vector3df &halfSize);

	void update();
private:

	irr::IrrlichtDevice& mDevice;
	std::map<irr::scene::IParticleSystemSceneNode*, float> mGlowings;
	std::map<irr::scene::IParticleSystemSceneNode*, float> mFire;

	std::vector<Globule> mGlobers;
	irr::scene::IAnimatedMesh *mGlobule;
	irr::core::vector3df mMinBox;
	irr::core::vector3df mMaxBox;

};

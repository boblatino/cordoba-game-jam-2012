#include "SpecialEffects.h"

#include <ISceneManager.h>
#include <IAnimatedMesh.h>
#include <IMesh.h>

#include <engine/IrrlichtGlobals.h>
#include <utils/DebugUtil.h>


irr::core::vector3df SpecialEffects::Globule::mMax;
irr::core::vector3df SpecialEffects::Globule::mMin;



 void
 SpecialEffects::Globule::update()
 {
     ASSERT(mNode);
     mNode->setPosition(mNode->getPosition() + mDirVec * 100.0f *
             IrrlichtGlobals::lastTimeFrame());
     mNode->setRotation(mNode->getRotation() + mRot);
     // check if it is outside of the box
     if (mNode->getPosition() > mMax || mNode->getPosition() < mMin) {
         mDirVec *= -1.f;
     }
 }

SpecialEffects::SpecialEffects(irr::IrrlichtDevice& device) 
    : mDevice(device) 
{
    irr::scene::ISceneManager& sceneManager = IrrlichtGlobals::sceneManager();
    mGlobule = sceneManager.getMesh("media/globules/globule.irrmesh");
    ASSERT(mGlobule != 0);
}

SpecialEffects::~SpecialEffects()
{
    // we let ramiro's leaks here :)
}

// Create an Explosion based on a series of bitmaps, animated sprites
void SpecialEffects::createGlowing(const irr::core::vector3df &pos)
{
	irr::scene::IParticleSystemSceneNode* ps = mDevice.getSceneManager()->addParticleSystemSceneNode(false);
	irr::scene::IParticleEmitter* em = ps->createBoxEmitter( 
		irr::core::aabbox3d<irr::f32>(-7,0,-7,7,1,7), // emitter size
		irr::core::vector3df(0.0f,0.06f,0.0f),   // initial direction
		80,100,                             // emit rate
		irr::video::SColor(0,255,255,255),       // darkest color
		irr::video::SColor(0,255,255,255),       // brightest color
		800,2000,0,                         // min and max age, angle
		irr::core::dimension2df(10.f,10.f),         // min size
		irr::core::dimension2df(20.f,20.f));        // max size

	ps->setEmitter(em); // this grabs the emitter
	em->drop(); // so we can drop it here without deleting it

	irr::scene::IParticleAffector* paf = ps->createFadeOutParticleAffector();

	ps->addAffector(paf); // same goes for the affector
	paf->drop();

	ps->setPosition(pos);
	ps->setScale(irr::core::vector3df(2,2,2));
	ps->setMaterialFlag(irr::video::EMF_LIGHTING, false);
	ps->setMaterialFlag(irr::video::EMF_ZWRITE_ENABLE, false);
	ps->setMaterialTexture(0, mDevice.getVideoDriver()->getTexture("media/explosion/1/1.bmp"));
	ps->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);

	mGlowings[ps] = TIME_TO_DISMISS_GLOWINGS;
}

void SpecialEffects::createFire(const irr::core::vector3df &pos)
{
	irr::scene::IParticleSystemSceneNode* ps = mDevice.getSceneManager()->addParticleSystemSceneNode(false);
	irr::scene::IParticleEmitter* em = ps->createBoxEmitter(
		irr::core::aabbox3d<irr::f32>(-1,0,-2,7,1,7), // emitter size
		irr::core::vector3df(0.0f,0.06f,0.0f),   // initial direction
		30,60,                             // emit rate
		irr::video::SColor(0,255,255,255),       // darkest colorw
		irr::video::SColor(0,255,255,255),       // brightest color
		800,2000,0,                         // min and max age, angle
		irr::core::dimension2df(1.f,1.f),         // min size
		irr::core::dimension2df(1.f,1.f));        // max size

	ps->setEmitter(em); // this grabs the emitter
	em->drop(); // so we can drop it here without deleting it

	irr::scene::IParticleAffector* paf = ps->createFadeOutParticleAffector();

	ps->addAffector(paf); // same goes for the affector
	paf->drop();

	ps->setPosition(pos);
	ps->setScale(irr::core::vector3df(2,2,2));
	ps->setMaterialFlag(irr::video::EMF_LIGHTING, false);
	ps->setMaterialFlag(irr::video::EMF_ZWRITE_ENABLE, false);
	ps->setMaterialTexture(0, mDevice.getVideoDriver()->getTexture("media/images/blood_1.png"));
	ps->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);

	mFire[ps] = TIME_TO_DISMISS_FIRE;
}

void
SpecialEffects::createGlobules(const int num, const irr::core::vector3df &halfSize)
{
    irr::core::vector3df zeroVec;
    irr::core::vector3df unHalf(halfSize);
    unHalf *= -1;

    if (halfSize > zeroVec) {
        Globule::mMax = mMaxBox = halfSize;
        Globule::mMin = mMinBox = unHalf;
    } else {
        Globule::mMax = mMaxBox = unHalf;
        Globule::mMin = mMinBox = halfSize;
    }

    ASSERT(mGlobule != 0);

    // construct num objects
    irr::scene::ISceneManager& sceneManager = IrrlichtGlobals::sceneManager();

    for (int i = 0; i < num; ++i){
        irr::scene::ISceneNode *node = sceneManager.addAnimatedMeshSceneNode(mGlobule);
        node->setScale(irr::core::vector3df(10,10,10));
        ASSERT(node);
        irr::core::vector3df pos;
        pos.X = (std::rand() % (std::abs(static_cast<int>(halfSize.X * 2)))) - halfSize.X;
        pos.Y = (std::rand() % (std::abs(static_cast<int>(halfSize.Y * 2)))) - halfSize.Y;
        pos.Z = (std::rand() % (std::abs(static_cast<int>(halfSize.Z * 2)))) - halfSize.Z;
        node->setPosition(pos);
        mGlobers.push_back(Globule(node));
    }
}



void SpecialEffects::update() {
	for (std::map<irr::scene::IParticleSystemSceneNode*, float>::iterator it = mGlowings.begin(); it != mGlowings.end(); ++it) {
		it->second -= IrrlichtGlobals::lastTimeFrame();
		if (it->second <= 0.0f) {
			it->first->remove();
			mGlowings.erase(it);
			break;
		}
	}

	for (std::map<irr::scene::IParticleSystemSceneNode*, float>::iterator it = mFire.begin(); it != mFire.end(); ++it) {
		it->second -= IrrlichtGlobals::lastTimeFrame();
		if (it->second <= 0.0f) {
			it->first->remove();
			mFire.erase(it);
			break;
		}
	}

	for(size_t i = 0, size = mGlobers.size(); i < size; ++i){
	    mGlobers[i].update();
	}
}

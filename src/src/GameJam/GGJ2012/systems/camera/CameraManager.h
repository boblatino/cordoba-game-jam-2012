/*
 * CameraManager.h
 *
 *  Created on: Jan 26, 2013
 *      Author: agustin
 */

#ifndef CAMERAMANAGER_H_
#define CAMERAMANAGER_H_

#include <ICameraSceneNode.h>

class Player;

class CameraManager {

    enum State {
        NONE = 0,
        TRACKING_PLAYER,
        EXTERNAL_ANIMATION, // for the future
    };

    static const float CAM_Z_PLAYER_DIST;
    static const float CAM_Y_PLAYER_ELEVATION;
public:
    // Construct the camera with the Player
    CameraManager(const Player *player);
    ~CameraManager();

    /**
     * Track player movement
     */
    inline void trackPlayer(void);


    /**
     * Update the camera manager
     */
    void update(void);

private:
    CameraManager(const CameraManager &);
    CameraManager &operator=(const CameraManager&);

    // Init new state
    void initState(State s);

private:
    const Player *mPlayer;
    irr::scene::ICameraSceneNode *mCameraNode;
    State mState;
};


inline void
CameraManager::trackPlayer(void)
{
    initState(TRACKING_PLAYER);
}

#endif /* CAMERAMANAGER_H_ */

/*
 * CameraManager.cpp
 *
 *  Created on: Jan 26, 2013
 *      Author: agustin
 */

#include "CameraManager.h"

#include <ISceneManager.h>

#include <systems/player/Player.h>
#include <utils/DebugUtil.h>




const float CameraManager::CAM_Z_PLAYER_DIST = 15.0f;
const float CameraManager::CAM_Y_PLAYER_ELEVATION = 10.0f;


void
CameraManager::initState(State s)
{
    // do nothing for now
    mState = s;
}


CameraManager::CameraManager(const Player *player) :
    mPlayer(player)
,   mCameraNode(IrrlichtGlobals::sceneManager().getActiveCamera())
,   mState(NONE)
{
    ASSERT(player);
}

CameraManager::~CameraManager()
{

}



void
CameraManager::update(void)
{
    switch(mState) {
    case State::NONE:
        return;

    case State::TRACKING_PLAYER:
    {
        // get the position of the player
        irr::core::matrix4 m;
        const irr::core::vector3df &pp = mPlayer->getPosition();

        m.setRotationDegrees(mPlayer->getRotation());
        irr::core::vector3df trans(0.0f,
                                   CAM_Y_PLAYER_ELEVATION,
                                   CAM_Z_PLAYER_DIST);
        m.transformVect(trans);
        ASSERT(mCameraNode != 0);
        mCameraNode->setPosition(pp + trans);
        mCameraNode->updateAbsolutePosition();

        // look at the object
        trans.X = pp.X;
        trans.Y = pp.Y + CAM_Y_PLAYER_ELEVATION;
        trans.Z = pp.Z;
        mCameraNode->setTarget(trans);
    }
    break;

    default:
        ASSERT(false);
    }
}


#pragma once

class ICollisionObject;

struct ICollision {
	virtual void collides(ICollisionObject* self, ICollisionObject* other) = 0;
};
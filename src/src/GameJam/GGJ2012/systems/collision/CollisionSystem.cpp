#include "CollisionSystem.h"


CollisionSystem::CollisionSystem(irrBulletWorld& world) 
    : mWorld(world) 
{
		
}

void CollisionSystem::registerCollisionObj(ICollision* collisionCB, ICollisionObject* obj) 
{
	mCollisionObjects[obj] = collisionCB;
}

void CollisionSystem::unregisterCollisionObj(ICollisionObject* obj) 
{
    mCollisionObjects.erase(obj);
}

void CollisionSystem::update()
{
    const size_t numManifolds = mWorld.getNumManifolds();
	for(size_t i=0; i < numManifolds; ++i)
	{
		ICollisionCallbackInformation *info = mWorld.getCollisionCallback(i);

        // Do the collision thing here
        const bool canBody0Affect = info->getBody0()->getAttributes()->existsAttribute("collide") && info->getBody0()->getAttributes()->getAttributeAsBool("collide");
        const bool canBody1Affect = info->getBody1()->getAttributes()->existsAttribute("collide") && info->getBody1()->getAttributes()->getAttributeAsBool("collide");
        if (canBody0Affect && canBody1Affect) {   
            // Check if the target was impacted by a bullet.
            const bool body0IsTarget = info->getBody0()->getAttributes()->existsAttribute("isTarget") && info->getBody0()->getAttributes()->getAttributeAsBool("isTarget");
            const bool body1IsTarget = info->getBody1()->getAttributes()->existsAttribute("isTarget") && info->getBody1()->getAttributes()->getAttributeAsBool("isTarget");
            const bool body0IsRegistered = mCollisionObjects.find(info->getBody0()) != mCollisionObjects.end();
            const bool body1IsRegistered = mCollisionObjects.find(info->getBody1()) != mCollisionObjects.end();

            if (body0IsTarget && body0IsRegistered && body1IsRegistered) {
                mCollisionObjects[info->getBody0()]->collides(info->getBody0(), info->getBody1()); 

                // We do not want to continue receiving collisions between target and this current body.
                //mCollisionObjects.erase(info->getBody1());
                info->getBody1()->getAttributes()->setAttribute("collide", false);

            } else if (body1IsTarget && body0IsRegistered && body1IsRegistered) {
                mCollisionObjects[info->getBody1()]->collides(info->getBody1(), info->getBody0()); 

                // We do not want to continue receiving collisions between target and this current body.
                //mCollisionObjects.erase(info->getBody0());
                info->getBody0()->getAttributes()->setAttribute("collide", false);
            } else {
                info->getBody0()->getAttributes()->setAttribute("collide", false);
                info->getBody1()->getAttributes()->setAttribute("collide", false);
            }
        }

		delete info;
	}
}
#pragma once

#include <irrbullet.h>
#include "ICollision.h"
#include <map>

class CollisionSystem {

public:
	CollisionSystem(irrBulletWorld& world); 
	
    void registerCollisionObj(ICollision* collisionCB, ICollisionObject* obj);
    void unregisterCollisionObj(ICollisionObject* obj);

	void update();

	void getContactPoint() {
	/* int numContacts = info->getPointer()->getNumContacts();
        for(int j=0; j < numContacts; j++)
        {
            if(verifyCollisionCallback(info))
            {
                if(info->getContactPoint(j).getDistance()<1.5f && info->getContactPoint(j).getLifeTime() < 2.0f)
                {
                    // Handle contact point
                }
            }
        }
        info->getPointer()->clearManifold();*/
	}
private:
	irrBulletWorld& mWorld;
	std::map<ICollisionObject*, ICollision*> mCollisionObjects;
};


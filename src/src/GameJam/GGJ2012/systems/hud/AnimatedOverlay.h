/*
 * AnimatedOverlay.h
 *
 *  Created on: Jan 26, 2013
 *      Author: agustin
 */

#ifndef ANIMATEDOVERLAY_H_
#define ANIMATEDOVERLAY_H_

#include <vector>

#include <IGUIImage.h>
#include <ITexture.h>

class AnimatedOverlay {
public:
    AnimatedOverlay();
    ~AnimatedOverlay();

    // configure
    void configure(irr::gui::IGUIImage *overlay,
                   const std::vector<irr::video::ITexture*> &textures,
                   float totalTime,
                   bool loop = false);

    void restart(float totalTime);
    void update(void);

private:
    irr::gui::IGUIImage *mOverlay;
    std::vector<irr::video::ITexture*> mTextures;
    float mTotalTime;
    float mAccumTime;
    bool mLoop;
    int mIndex;
};

#endif /* ANIMATEDOVERLAY_H_ */

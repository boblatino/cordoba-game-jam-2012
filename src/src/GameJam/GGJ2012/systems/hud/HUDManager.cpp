/*
 * HUDManager.cpp
 *
 *  Created on: Jan 26, 2013
 *      Author: agustin
 */

#include "HUDManager.h"

#include <cmath>

#include <ik_ISound.h>
#include <ik_ISoundEngine.h>

#include <IBillboardSceneNode.h>
#include <ICursorControl.h>
#include <IGUIEnvironment.h>
#include <IGUIImage.h>
#include <IMeshManipulator.h>
#include <IMeshSceneNode.h>
#include <IParticleSystemSceneNode.h>
#include <IrrlichtDevice.h>
#include <ISceneManager.h>
#include <IVideoDriver.h>

#include <engine/IrrlichtGlobals.h>

#include <sound/SoundEngine.h>

#include <utils/DebugUtil.h>

#include <cstdint>
/*
void
HUDManager::BulletHandling::renderElements(
        const std::vector<irr::gui::IGUIImage *> &elements)
{
    ASSERT(elements.size() == HUDManager::BulletHandling::SIZE);
    for (size_t i = 0; i < HUDManager::BulletHandling::SIZE; ++i){
        elements[i]->setRelativePosition(mPositions[i]);
    }
}


HUDManager::BulletHandling::BulletHandling()
{

}
HUDManager::BulletHandling::~BulletHandling()
{
    for(size_t i = 0; i < HUDManager::BulletHandling::SIZE; ++i){
        mImages[i]->remove();
    }
}

// configure the size in the window (relative)
//
void
HUDManager::BulletHandling::configure(const irr::core::rect<irr::s32>& r)
{
    // get the size of each picture
    const float picSize = r.getWidth() / HUDManager::BulletHandling::SIZE;

    // set the sizes of each box
    const float y = r.UpperLeftCorner.Y;
    for (size_t i = 0; i < HUDManager::BulletHandling::SIZE; ++i){
        const float x = i * picSize + r.UpperLeftCorner.X;
        mPositions[i].UpperLeftCorner.X = x;
        mPositions[i].UpperLeftCorner.Y = y;
        mPositions[i].LowerRightCorner.X = x + picSize;
        mPositions[i].LowerRightCorner.Y = y + r.getHeight();
    }

    // load here the images
    irr::gui::IGUIEnvironment& gui = IrrlichtGlobals::guiEnvironment();
    irr::video::IVideoDriver& videoDrv = IrrlichtGlobals::videoDriver();
    mImages[0] = gui.addImage(mPositions[0]);
    mImages[0]->setImage(videoDrv.getTexture("media/bullets/b1.png"));
    mImages[0]->setUseAlphaChannel(true);
    mImages[0]->setScaleImage(true);

    mImages[1] = gui.addImage(mPositions[1]);
    mImages[1]->setImage(videoDrv.getTexture("media/bullets/b2.png"));
    mImages[1]->setUseAlphaChannel(true);
    mImages[1]->setScaleImage(true);

    mImages[2] = gui.addImage(mPositions[2]);
    mImages[2]->setImage(videoDrv.getTexture("media/bullets/b3.png"));
    mImages[2]->setUseAlphaChannel(true);
    mImages[2]->setScaleImage(true);

    // push this piece of crap into the vector
    for (size_t i = 0; i < HUDManager::BulletHandling::SIZE; ++i){
        mBulletGUIS.push_back(mImages[i]);
    }

}

// remove first (left one)
void
HUDManager::BulletHandling::removeFirst(void)
{
    mBulletGUIS.erase(mBulletGUIS.begin());
    mBulletGUIS.push_back(mImages[std::rand() %
                                  HUDManager::BulletHandling::SIZE]);
    renderElements(mBulletGUIS);
}

// get first (left one)
irr::gui::IGUIImage *
HUDManager::BulletHandling::getFirst(void) const
{
    return mBulletGUIS.front();
}

HUDManager::BulletType
HUDManager::BulletHandling::getFirstType(void) const
{
    irr::gui::IGUIImage *first = getFirst();
    for (int i = 0; i < HUDManager::BulletHandling::SIZE; ++i){
        if (first == mImages[i]) {
            return HUDManager::BulletType(i);
        }
    }
    ASSERT(false);

    return BulletType::T_1;
}*/



 // Build the ui for the bullets
void
HUDManager::buildBullets()
{

}

// build the heart anim
void
HUDManager::buildHeartAnim()
{

}

void
HUDManager::calculateTimes(void)
{
    mNumBeats = STARTING_NUM_BEATS;
    mThresholdSize = mRemainingTime / static_cast<float>(NUM_CHANGES);
    for (size_t i = 0; i < NUM_CHANGES; ++i) {
        for(size_t j = 0, size = mNumBeats * 2 - 1; j < size; ++j){
            mSoundsToReproduce.push_back(HUDManager::FakeSound(
                    mThresholdSize/static_cast<float>(size), (j % 2) == 0));
        }
        mNumBeats++;
    }
}

float HUDManager::calculateSampleRate(irrklang::ISound* sound, const float desiredLength)
{
    const uint32_t trackLength = sound->getPlayLength();
    const float sampleRate = desiredLength / trackLength;

    return sampleRate;
}

HUDManager::HUDManager() :
    mRunning(false)
,   mCallback(0)
,   mRemainingTime(0.0f)
,   mAccumTime(0.0f)
,   mThresholdSize(0.0f)
,   mHeartOverlay(0)
,   mSoundSource(nullptr)
,   mSound(nullptr)
,   mSampleRate(0.50f)
{
    irr::IrrlichtDevice *device = &IrrlichtGlobals::device();
    // load the heart anim sprites
    std::vector<irr::video::ITexture*> sprites;
    char tmp[64];
    for (uint32_t d = 0; d <= 9; ++d)
    {
        sprintf(tmp, "media/sprites/heart_0_0000%d.png", d);
        irr::video::ITexture* t = device->getVideoDriver()->getTexture(tmp);

        // Add one more sprite in the array
        sprites.push_back(t);
    }

    irr::video::ITexture* t = device->getVideoDriver()->getTexture(
            "media/sprites/heart_0_00010.png");
    sprites.push_back(t);

    t = device->getVideoDriver()->getTexture("media/sprites/heart_0_00011.png");
    sprites.push_back(t);

    irr::gui::IGUIEnvironment& gui = IrrlichtGlobals::guiEnvironment();
    irr::video::IVideoDriver& videoDrv = IrrlichtGlobals::videoDriver();
    mHeartOverlay = gui.addImage(irr::core::rect<irr::s32>(100, 600, 200, 700));
    mHeartOverlay->setUseAlphaChannel(true);
    mHeartOverlay->setScaleImage(true);
    mHeartAnim.configure(mHeartOverlay, sprites, 0, true);

    // Configure the BulletHandling position
    //mBulletHandler.configure(irr::core::rect<irr::s32>(700, 600, 900, 700));

    // Load sound
    mSoundSource = sound::SoundEngine::soundEngine().addSoundSourceFromFile("media/sounds/heartbeat.wav");
}

HUDManager::~HUDManager()
{
    mHeartOverlay->remove();
}

/**
* Pop one of the actual shoots types (this will create a new one randomly)
* @returns the new "current" type of the bullet
*//*
HUDManager::BulletType
HUDManager::popShootType(void)
{
    //mBulletHandler.removeFirst();
   // return mBulletHandler.getFirstType();
}*/

/**
* Configure the time of the level (the time that will said when the heart
* will stop beating).
*/
void
HUDManager::setTimeLevel(const float tl)
{
    mRemainingTime = tl;
}

/**
* Start to run the timer for the heart...
*/
void
HUDManager::start(void)
{
    if (mRemainingTime <= 0.0f) {
        ASSERT(false);
        return;
    }
    mSoundsToReproduce.clear();
    mRunning = true;
    mNumBeats = STARTING_NUM_BEATS;
    calculateTimes();

}

/**
* Hide / show the hud
*/
void
HUDManager::setVisible(bool visible)
{
    ASSERT(false);
}

/**
* Update the HUD in each frame
*/
bool
HUDManager::update()
{
    if (!mRunning) {
       return false;
    }

    // check if we finish the animation
    mRemainingTime -= IrrlichtGlobals::lastTimeFrame();
    if (mRemainingTime <= 0.0f || mSoundsToReproduce.empty()) {
        // we finish
        mRunning = false;
        mSoundsToReproduce.clear();
        if (mCallback != 0) {            
            (*mCallback)(EventType::E_HEART_DIE);            
        }

        return true;
    }

    // get the sound to reproduce
    FakeSound &sound = mSoundsToReproduce.front();
    if (sound.reproduce) {
        sound.reproduce = false;

        // configure the animation velocity
        mHeartAnim.restart(sound.time);
        // this will not work perfectly (will animate more times that the sound
        // does
    }

    sound.time -= IrrlichtGlobals::lastTimeFrame();
    if (sound.time < 0.0f) {
        // this sound should be removed
        mSoundsToReproduce.pop_front();
    }
    mHeartAnim.update();

    if (!mSound || mSound->isFinished())
    {
        mSound = sound::SoundEngine::soundEngine().play2D(mSoundSource, false, false, false, true);
        if (mSound) {
            mSampleRate += 0.25f;
            mSound->setPlaybackSpeed(mSampleRate);
        }
    }

    return false;
}


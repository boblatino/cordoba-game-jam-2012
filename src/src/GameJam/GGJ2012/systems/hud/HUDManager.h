/*
 * HUDManager.h
 *
 *  Created on: Jan 26, 2013
 *      Author: agustin
 */

#ifndef HUDMANAGER_H_
#define HUDMANAGER_H_

#include <irrArray.h>
#include <IGUIElement.h>
#include <IGUIImage.h>
#include <ITexture.h>
#include <ISceneNodeAnimator.h>

#include <deque>
#include <vector>

#include "AnimatedOverlay.h"

namespace irrklang
{
    class ISound;
    class ISoundSource;
}

class HUDManager {

    // defines
    //
    static const float HEART_ANIM_TIME;
    static const float HEART_SOUND_TIME;
    static const int NUM_CHANGES = 6;   // the times that the heart beat will be
                                        // incremented over all the execution time
    static const int STARTING_NUM_BEATS = 4;


public:

    enum EventType {
        E_HEART_DIE,
    };

    enum BulletType {
        // Sync this with the other enum
        T_1,
        T_2,
        T_3,
        SIZE
    };
    // Callback type
    class HUDCallback {
    public:
        virtual void operator()(EventType eventType) = 0;
    };
public:
    HUDManager();
    ~HUDManager();

    /**
     * Pop one of the actual shoots types (this will create a new one randomly)
     * @returns the new "current" type of the bullet
     */
    //BulletType popShootType(void);

    /**
     * Configure the time of the level (the time that will said when the heart
     * will stop beating).
     */
    void setTimeLevel(const float tl);

    /**
     * Start to run the timer for the heart...
     */
    void start(void);

    /**
     * Hide / show the hud
     */
    void setVisible(bool visible);

    /**
     * Update the HUD in each frame
     */
    bool update();

    /**
     * Set a callback
     */
    void setCallback(HUDCallback *cb) {mCallback = cb;}


private:
    struct FakeSound {
        FakeSound(float t = 0.0f, bool r = false) : time(t), reproduce(r){}
        float time;
        bool reproduce;
    };

    // Bullet gui handling
    /*class BulletHandling {
        static const int SIZE = 3;

    public:

    public:
        BulletHandling();
        ~BulletHandling();

        // configure the size in the window (relative)
        // This will also load the images
        void configure(const irr::core::rect<irr::s32>& r);

        // remove first (left one) and automatically generates a new one
        void removeFirst(void);

        // get first (left one)
        irr::gui::IGUIImage *getFirst(void) const;

        // returns the type of the first bullet
        BulletType getFirstType(void) const;


    private:
        // render a vector of IGUIImages
        void renderElements(const std::vector<irr::gui::IGUIImage *> &elemts);

    private:
        irr::core::rect<irr::s32> mPositions[SIZE];
        irr::gui::IGUIImage *mImages[SIZE];
        std::vector<irr::gui::IGUIImage *> mBulletGUIS;
    };*/
private:
    HUDManager(const HUDManager&);
    HUDManager &operator=(const HUDManager&);


    // Build the ui for the bullets
    void buildBullets();

    // build the heart anim
    void buildHeartAnim();

    // Calculate vector of times for all the game
    void calculateTimes();

    float calculateSampleRate(irrklang::ISound* sound, const float desiredLength);

private:
    HUDCallback *mCallback;
    bool mRunning;
    float mRemainingTime;
    float mAccumTime;
    float mThresholdSize;
    int mNumBeats;
    float mSoundLength;
    std::deque<FakeSound> mSoundsToReproduce;
    //BulletHandling mBulletHandler;
    AnimatedOverlay mHeartAnim;
    irr::gui::IGUIImage *mHeartOverlay;

    irrklang::ISoundSource* mSoundSource;
    irrklang::ISound* mSound;
    float mSampleRate;
};

#endif /* HUDMANAGER_H_ */

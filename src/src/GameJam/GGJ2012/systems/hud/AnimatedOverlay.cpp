/*
 * AnimatedOverlay.cpp
 *
 *  Created on: Jan 26, 2013
 *      Author: agustin
 */

#include "AnimatedOverlay.h"

#include <engine/IrrlichtGlobals.h>

#include <utils/DebugUtil.h>

AnimatedOverlay::AnimatedOverlay() :
mAccumTime(0),
mTotalTime(0),
mIndex(0),
mOverlay(0)
{

}

AnimatedOverlay::~AnimatedOverlay()
{

}

void
AnimatedOverlay::configure(irr::gui::IGUIImage *overlay,
               const std::vector<irr::video::ITexture*> &textures,
               float totalTime,
               bool loop)
{
    mTotalTime = totalTime;
    mAccumTime = 0.0f;
    mLoop = loop;
    mOverlay = overlay;
    ASSERT(mOverlay);
    mTextures = textures;
    mIndex = 0;

    if (!mTextures.empty()){
        mOverlay->setImage(mTextures.front());
    }
}

void
AnimatedOverlay::restart(float totalTime)
{
    mAccumTime = 0.0f;
    mIndex = 0;
    mTotalTime = totalTime;
}

void
AnimatedOverlay::update(void)
{
    mAccumTime += IrrlichtGlobals::lastTimeFrame();
    if (mAccumTime >= mTotalTime) {
        if (mLoop) {
            mAccumTime = 0;
        } else {
            return;
        }
    }
    // check which should be the index
    const int index = static_cast<int>(mAccumTime/mTotalTime * mTextures.size()-1);
    if (mIndex != index && index < mTextures.size()) {
        // configure the new index
        ASSERT(mOverlay != 0);
        ASSERT(mIndex < mTextures.size());
        mOverlay->setImage(mTextures[mIndex]);
        mIndex = index;

    }
}

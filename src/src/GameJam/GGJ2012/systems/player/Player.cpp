/*
 * Player.cpp
 *
 *  Created on: Jan 25, 2013
 *      Author: agustin
 */

#include "Player.h"

#include <ISceneManager.h>
#include <IAnimatedMeshSceneNode.h>

#include <utils/DebugUtil.h>

const char *Player::SHIP_NAME = "media/player/playerShip.irrmesh";
const char *Player::TORRET_NAME = "media/player/playerTorret.irrmesh";

const float Player::Y_HEIGHT_SHIPE = 10.0f; 

// offset of the torrent from the ship
const float Player::Y_TORRET_OFFSET = 5.0f;

// MAX and MIN rotation degrees of the turret
const float Player::MAX_TURRET_ROT = 45.0f; // PI/2
const float Player::MIN_TURRET_ROT = 0.0f;

Player::Player() :
    mShipNode(0)
,   mTorretNode(0)
,   mVelocity(1.0f)
,   mTurretVelocity(1.0f)
,   mFarCSqr(0)
,   mNearCSqr(0)
{
    // construct the scene nodes
    irr::scene::ISceneManager& sceneManager = IrrlichtGlobals::sceneManager();
    irr::scene::IAnimatedMesh *shipMesh = sceneManager.getMesh(SHIP_NAME);
    ASSERT(shipMesh);
    mShipNode = sceneManager.addAnimatedMeshSceneNode(shipMesh);
    ASSERT(mShipNode);

    irr::scene::IAnimatedMesh *torretMesh = sceneManager.getMesh(TORRET_NAME);
    ASSERT(torretMesh);
    mTorretNode = sceneManager.addAnimatedMeshSceneNode(torretMesh);
    ASSERT(mTorretNode);

    // set the torret as a child of the ship in a hardcoded position
    irr::core::vector3df pos = mTorretNode->getPosition();
    pos.Y += Y_TORRET_OFFSET;
    mTorretNode->setPosition(pos);
    mShipNode->addChild(mTorretNode);

    pos = mShipNode->getPosition();
    pos.Y = Y_HEIGHT_SHIPE;
    mShipNode->setPosition(pos);
}

Player::~Player()
{
    // probably some drops here

}

void
Player::rotateShip(const float rot)
{
    const float nextY = mShipNode->getRotation().Y + rot * mVelocity;
    irr::core::vector3df r;
    r.Y = nextY;
    mShipNode->setRotation(r);

}

void
Player::translate(const irr::core::vector3df &t)
{
    irr::core::matrix4 m;

    m.setRotationDegrees(mShipNode->getRotation());

    irr::core::vector3df trans(t * mVelocity);
    m.transformVect(trans);
    trans += mShipNode->getPosition();

    // check if the position is safe (between near and far)
    const float sqrDist = trans.getLengthSQ();
    if (sqrDist > mFarCSqr || sqrDist < mNearCSqr) {
        // dont move the player
        return;
    }

    mShipNode->setPosition(trans);
    mShipNode->updateAbsolutePosition();
}

void
Player::rotateTurret(const float rot)
{
    const float nextRot = (mTorretNode->getRotation().X + rot) * mTurretVelocity;
    if (nextRot >= MAX_TURRET_ROT || nextRot <= MIN_TURRET_ROT){
        return;
    }
    irr::core::vector3df r = mTorretNode->getRotation();
    r.X = nextRot;
    mTorretNode->setRotation(r);
}

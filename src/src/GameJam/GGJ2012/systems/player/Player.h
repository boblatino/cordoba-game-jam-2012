/*
 * Player.h
 *
 *  Created on: Jan 25, 2013
 *      Author: agustin
 */

#ifndef PLAYER_H_
#define PLAYER_H_

#include <vector3d.h>
#include <ISceneNode.h>
#include <engine/IrrlichtGlobals.h>

class Player {
    // define the name of the mesh
    static const char *SHIP_NAME;
    static const char *TORRET_NAME;

    // flying height of the ship
    static const float Y_HEIGHT_SHIPE;

    // offset of the torrent from the ship
    static const float Y_TORRET_OFFSET;

    // MAX and MIN rotation degrees of the turret
    static const float MAX_TURRET_ROT;
    static const float MIN_TURRET_ROT;


public:
    Player();
    ~Player();

    /**
     * Set the position/translation/etc of the player (ship)
     */
    inline void setPosition(const irr::core::vector3df &pos);
    inline const irr::core::vector3df &getPosition() const;
    inline void setRotation(const irr::core::vector3df &rot);
    inline const irr::core::vector3df &getRotation() const;

    /**
     * Rotate the ship in axis Y
     */
    void rotateShip(const float rot);

    /**
     * Translate the ship for where it is pointing to
     */
    void translate(const irr::core::vector3df &t);

    /**
     * Rotate the turret only in x axis in degrees
     */
    void rotateTurret(const float rot);

    /**
     * Set the velocity for movement and turret movement
     */
    inline void setVelocity(const float vel);
    inline float getVelocity();
    inline void setTurretVelocity(const float vel);
    inline float getTurretVelocity();

    inline irr::core::vector3df getLinearVelocityAccordingTurret();

    inline const irr::core::vector3df &
    getTurretPosition() const
    {
        return mTorretNode->getPosition();
    }
 // configure the two circumferences where the player will be able to move
    // and we will assume the level will be on x=0=z
    inline void configureMovemenetZone(const float nearC, const float farC);


private:
    Player(const Player&);
    Player &operator=(const Player &);

private:
    irr::scene::ISceneNode *mShipNode;
    irr::scene::ISceneNode *mTorretNode;
    float mVelocity;
    float mTurretVelocity;
    float mFarCSqr;
    float mNearCSqr;
};


inline void
Player::setPosition(const irr::core::vector3df &pos)
{
    mShipNode->setPosition(pos);
}

inline const irr::core::vector3df &
Player::getPosition() const
{
    return mShipNode->getPosition();
}

inline void
Player::setRotation(const irr::core::vector3df &rot)
{
    mShipNode->setRotation(rot);
}

inline const irr::core::vector3df &
Player::getRotation() const
{
    return mShipNode->getRotation();
}


inline void
Player::setVelocity(const float vel)
{
    mVelocity = vel;
}

inline float
Player::getVelocity()
{
    return mVelocity;
}

inline void
Player::setTurretVelocity(const float vel)
{
    mTurretVelocity = vel;
}

inline float
Player::getTurretVelocity()
{
    return mTurretVelocity;
}

inline void
Player::configureMovemenetZone(const float nearC, const float farC)
{
    mFarCSqr = farC*farC;
    mNearCSqr = nearC*nearC;
}

inline irr::core::vector3df 
Player::getLinearVelocityAccordingTurret()
{
    irr::core::vector3df turretRotation = mTorretNode->getRotation();
    irr::core::vector3df shipRotation = mShipNode->getRotation();
    irr::core::vector3df finalRotation(turretRotation.X, shipRotation.Y, 0);
    irr::core::matrix4 rotationMatrix; 
    rotationMatrix.setRotationDegrees(finalRotation);
    
    irr::core::vector3df translation(0.0f, 0.0f, -120.0f);
    rotationMatrix.transformVect(translation);
    
    //irr::core::vector3df forwardDir(irr::core::vector3df(sphereRotation[8], sphereRotation[9], sphereRotation[10]) * 120);

    return translation;
}

#endif /* PLAYER_H_ */

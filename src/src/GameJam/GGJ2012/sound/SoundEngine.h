#pragma once

#include <irrKlang.h>

namespace irrKlang
{
    class ISoundEngine;
}

namespace sound
{
    class SoundEngine
    {
    public:
        static void init()
        {
            mSoundEngine = irrklang::createIrrKlangDevice();
        }

        static void destroy()
        {
            mSoundEngine->drop();
        }

        static irrklang::ISoundEngine& soundEngine() { return *mSoundEngine; }
    private:
        static irrklang::ISoundEngine* mSoundEngine;

        SoundEngine();
        SoundEngine(const SoundEngine& soundEngine);
        SoundEngine& operator=(const SoundEngine& soundEngine);
    };
}

#pragma once

namespace ai
{
    class PathManager;
}

namespace enemy
{
    class EnemyManager; 
}

namespace game
{
    class GamePlayer;
}

namespace item
{
    class ItemManager;
}

namespace weapon
{
    class WeaponManager;
}

namespace utils
{
    class FXManager;
}

class BulletGeneratorSystem;
class InputSystem;
class SceneLoader;
class SpecialEffects;

class Globals
{
public:
    static void initAll();
    static void destroyAll();

    static utils::FXManager& fxManager() { return *mFXManager; }
    static game::GamePlayer& gamePlayer() { return *mGamePlayer; }
    static InputSystem& inputSystem() { return *mInputSystem; }
    static SceneLoader& sceneLoader() { return *mSceneLoader; }
    static BulletGeneratorSystem& bulletGeneratorSystem() { return *mBulletGeneratorSystem; }
    static SpecialEffects& specialEffects() { return *mSpecialEffects; }

private:
    static utils::FXManager* mFXManager;
    static game::GamePlayer* mGamePlayer;
    static InputSystem* mInputSystem;
    static SceneLoader* mSceneLoader;
    static BulletGeneratorSystem* mBulletGeneratorSystem;
    static SpecialEffects* mSpecialEffects;

    Globals();
    Globals(const Globals& globals);
    Globals& operator=(const Globals& globals);
};
#include "Globals.h"

#include <engine/IrrlichtGlobals.h>

#include <game/EntitiesID.h>
#include <game/GamePlayer.h>

#include <systems/bulletGenerator/BulletGeneratorSystem.h>
#include <systems/loader/SceneLoader.h>
#include <systems/specialEffects/SpecialEffects.h>

#include <utils/FXManager.h>
#include <utils/InputSystem.h>

utils::FXManager* Globals::mFXManager = nullptr;
game::GamePlayer* Globals::mGamePlayer = nullptr;
InputSystem* Globals::mInputSystem = nullptr;
SceneLoader* Globals::mSceneLoader = nullptr;
BulletGeneratorSystem* Globals::mBulletGeneratorSystem = nullptr;
SpecialEffects* Globals::mSpecialEffects = nullptr;

void Globals::initAll()
{
    mFXManager = new utils::FXManager();
    mGamePlayer = new game::GamePlayer(game::EntitiesIds::mPlayerId);
    mInputSystem = new InputSystem();
    mSceneLoader = new SceneLoader();
    mBulletGeneratorSystem = new BulletGeneratorSystem();
    mSpecialEffects = new SpecialEffects(IrrlichtGlobals::device());
}

void Globals::destroyAll()
{
    delete mFXManager;
    delete mGamePlayer;
    delete mInputSystem;
    delete mSceneLoader;
    delete mBulletGeneratorSystem;
    delete mSpecialEffects;
}

#include "IrrlichtGlobals.h"

#include <cstdint>

#include <irrlicht.h>
#include <IrrlichtDevice.h>

#include <game/GameManager.h>

irr::IrrlichtDevice* IrrlichtGlobals::mDevice = 0;
irr::gui::IGUIEnvironment* IrrlichtGlobals::mGuiEnvironment = 0;
irr::video::IVideoDriver* IrrlichtGlobals::mVideoDriver = 0;
irr::ITimer* IrrlichtGlobals::mTimer = 0;
irr::gui::ICursorControl* IrrlichtGlobals::mCursorControl = 0;
irr::scene::ISceneManager* IrrlichtGlobals::mSceneManager = 0;
irr::io::IFileSystem* IrrlichtGlobals::mFileSystem = 0;
irr::scene::ISceneCollisionManager* IrrlichtGlobals::mCollisionManager = 0;
float IrrlichtGlobals::sLastTimeFrame = 0.0f;

void IrrlichtGlobals::initAll()
{
	mDevice = irr::createDevice(irr::video::EDT_OPENGL, irr::core::dimension2d<uint32_t>( 
	        1024, 768), 32, false, false, false, &game::gameManager());
    mGuiEnvironment = mDevice->getGUIEnvironment();
    mVideoDriver = mDevice->getVideoDriver();
    mTimer = mDevice->getTimer();
    mCursorControl = mDevice->getCursorControl();
    mSceneManager = mDevice->getSceneManager();
    mFileSystem = mDevice->getFileSystem();
    mCollisionManager = mSceneManager->getSceneCollisionManager();
}

void IrrlichtGlobals::destroyAll()
{
    mDevice->drop();
}

#include "Game.h"

#include <IrrlichtDevice.h>

#include <game/GameManager.h>

#include <engine/IrrlichtGlobals.h>

namespace game
{
    // Main game loop
    void Game::run()
    {
        IrrlichtGlobals::initAll();
        game::gameManager().init();

        // Keep running game loop if device exists
        irr::IrrlichtDevice& irrlichtDevice = IrrlichtGlobals::device();
        irr::ITimer& timer = IrrlichtGlobals::timer();
        uint32_t timeThisFrame = timer.getTime();

        while(irrlichtDevice.run())
        {
            // calculate time frame
            const uint32_t now =  timer.getTime();
            IrrlichtGlobals::lastTimeFrame() = static_cast<float>(
                    (now - timeThisFrame)) * 0.001f;
            timeThisFrame = now;

            if (irrlichtDevice.isWindowActive()){
                game::gameManager().update();
            }
        }

        game::gameManager().clear();
        IrrlichtGlobals::destroyAll();
    }
}

#pragma once

#include <cstdint>
#include <limits>

#include <vector3d.h>

class GameManager;

namespace irr
{
	namespace scene
	{
		class IAnimatedMeshSceneNode;
	}
}

namespace game
{
    // The basic game building block, all game objects should inherit 
    // from here. 
    class IGameEntity
    {
    public:
        IGameEntity(const uint32_t id) 
            : mOffset(irr::core::vector3df(0.0f, 0.0f, 0.0f))
            , mId(id)
            , mTriggerRegion(0)
            , mMeshNode(0)
            , mWasTriggered(false)
        {

        }

        IGameEntity(IGameEntity&& gameEntity)
            : mOffset(gameEntity.mOffset)
            , mId(gameEntity.mId)
            , mTriggerRegion(gameEntity.mTriggerRegion)
            , mMeshNode(gameEntity.mMeshNode)
            , mWasTriggered(gameEntity.mWasTriggered)
        {
            gameEntity.mId = std::numeric_limits<uint32_t>::max();
            gameEntity.mMeshNode =  0;
        }

        virtual ~IGameEntity() {};

    public:
        //all entities must implement an update function
        virtual void update() = 0;

        uint32_t id() const { return mId; }

    private:
        // This must be called within the constructor to make sure the ID is set
        // correctly. 
        void setID(const uint32_t id)
        {
            //make sure the val is equal to or greater than the next available ID
            mId = id;
            mWasTriggered = true;
        }

    public:
        // Offset of the position for the model
        // of this entity. When you position the entity model at
        // some waypoint, the model can be at some incorrect position.
        // This value is needed to correct this kind of errors
        // when you add this value positioning the entity.
        irr::core::vector3df mOffset;
    private:
        // Every entity must have a unique identifying number
        uint32_t mId;

    public:
        // Trigger radius of the Item. Use a 
        // larger number for area triggers, doors, sound effects etc
        uint32_t mTriggerRegion;

        irr::scene::IAnimatedMeshSceneNode *mMeshNode;

    private:
        bool mWasTriggered;
    };
}

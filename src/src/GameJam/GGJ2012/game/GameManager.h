//////////////////////////////////////////////////////////////////////////
// Desc:   The Game manager controls the flow of the game. It also interfaces with 
//         the Irrlicht rendering engine, perforing generic game initialization and 
//         setup.
//         The game manager holds a pointer to a GameState object. Therefore, when 
//         it calls the Update function of the GameManager, it will simply redirect 
//         the call to the current state object.
//
//         An important thing to understand is that the GameManager object doesn�t 
//         know a thing about the state of the game. It is the GameState subclasses 
//         that will define the game logic and each state transition as the game goes 
//         by.   
//
////////////////////////////////////////////////////////////////////////////////

#pragma once

#include <cstdint>

#include <IEventReceiver.h>

#include <engine/Globals.h>

#include <gameStates/GameStates.h>
#include <gameStates/IntroState.h>
#include <gameStates/PresentationState.h>
#include <gameStates/levels/LevelState1.h>
#include <gameStates/levels/LevelState2.h>
#include <gameStates/MenuState.h>

#include <sound/SoundEngine.h>

namespace gameStates
{
    class IGameState;
}

namespace irr
{
	struct SEvent;
}

namespace game
{
    class GameManager : public irr::IEventReceiver
    {
    public:
        GameManager() 
            : mGameState(0)
            , mNextState(0)
            , mTimePreviousFrame(0) 
                 
        {

        }

        void init()
        {
            // Init all states
            gameStates::GameStates::initAll(); 

            Globals::initAll();
            
            sound::SoundEngine::init();

            // By default, we start with the introduction mode...
            changeState(gameStates::GameStates::presentationState());
        }

        void update();
        void clear()
        {
            gameStates::GameStates::destroyAll();    
            Globals::destroyAll();
            sound::SoundEngine::destroy();
        }

        bool OnEvent(const irr::SEvent &event);

        void changeState(gameStates::IGameState &state);

    private:
        gameStates::IGameState *mGameState; // Current state of the game.
        uint32_t mTimePreviousFrame; // Time when the previous frame started
        gameStates::IGameState *mNextState;
    
    };

    GameManager& gameManager();
}

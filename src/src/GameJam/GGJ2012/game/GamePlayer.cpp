#include <algorithm>
#include <cassert>

#include <ICameraSceneNode.h>
#include <IrrlichtDevice.h>
#include <IAnimatedMeshSceneNode.h>
#include <ISceneManager.h>

#include <game/GameManager.h>

#include <engine/IrrlichtGlobals.h>

#include "GamePlayer.h"

namespace game
{
    void GamePlayer::init(const uint32_t lives, const uint32_t health, const uint32_t maxHealth)
    {
        mMaxHealth = maxHealth;
        mHealth = std::min(health, mMaxHealth);

        mDeathTime = 0;

        mLockPosition.set(irr::core::vector3df(0.0f, 0.0f, 0.0f));
    }

    // Kill the player if his health is 0
    bool GamePlayer::kill()
    {
        if(mHealth == 0)
        {
            // Initialize player death time
            irr::ITimer& timer = IrrlichtGlobals::timer();
            if(mInitDeathTime)
            {
                mDeathTime = timer.getTime();
                mInitDeathTime = false;
            }

            // lock camera in position
            irr::scene::ISceneManager& sceneMgr = IrrlichtGlobals::sceneManager();
            if (mLockPosition == irr::core::vector3df(0,0,0)) 
            {
                // re-position the weapon(s)
                const irr::core::list<irr::scene::ISceneNode*> &camChildren =
                        sceneMgr.getActiveCamera()->getChildren();
                irr::core::list<irr::scene::ISceneNode*>::ConstIterator it =
                        camChildren.begin(),
                        endIt = camChildren.end();
                for(;it != endIt; ++it){
                    irr::scene::ISceneNode* sceneNode = *it;
                    reinterpret_cast<irr::scene::IAnimatedMeshSceneNode*>(sceneNode)->setVisible(true);
                }


                // change camera
                sceneMgr.setActiveCamera(fixPlayerCam());
            }

            sceneMgr.getActiveCamera()->setPosition(mLockPosition);

            // get that sinking feeling
            uint32_t currentTime = timer.getTime();
            if(currentTime - mDeathTime < smWaitingTime / 10) 
            {
                mLockPosition.Y -= 2;
                irr::scene::ISceneNodeAnimator * const anim = sceneMgr.createRotationAnimator(irr::core::vector3df(0, -0.050f, 0));	
                sceneMgr.getActiveCamera()->addAnimator(anim);
                anim->drop();
            }

            else
                sceneMgr.getActiveCamera()->removeAnimators();

            return (currentTime - mDeathTime > smWaitingTime);
        }

        else
            return false;
    }

    irr::scene::ICameraSceneNode* GamePlayer::fixPlayerCam()
    {
        irr::scene::ISceneManager& sceneMgr = IrrlichtGlobals::sceneManager();

        mLockPosition.set(sceneMgr.getActiveCamera()->getPosition());
        mLockRotation.set(sceneMgr.getActiveCamera()->getRotation());
        irr::scene::ICameraSceneNode * const deathCam = sceneMgr.addCameraSceneNodeFPS(0,8.0f,0.5f,-1);
        deathCam->setPosition(mLockPosition);
        deathCam->setRotation(mLockRotation);

        return deathCam;
    }
}

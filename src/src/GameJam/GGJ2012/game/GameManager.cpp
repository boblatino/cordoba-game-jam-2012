#include "GameManager.h"

#include <ICursorControl.h>
#include <IGUIEnvironment.h>
#include <ISceneManager.h>
#include <ITimer.h>
#include <IVideoDriver.h>

#include <engine/IrrlichtGlobals.h>
#include <utils/InputSystem.h>
#include <gameStates/IGameState.h>

namespace game
{
    void GameManager::update()
    {
        if (mGameState)
            mGameState->update();

        if (mNextState)
        {
            if (mGameState)
                mGameState->clear();

            // I verify that the state is a new one.
            if (mNextState != mGameState)
            {
                mGameState = mNextState;

                // Call enter state function
                mGameState->init();
            }

            mNextState = nullptr;
        }
    }

    // Main event handler derived from IEventHandler, this 
    // will be passed down to the current states keyboard handler.
    bool GameManager::OnEvent(const irr::SEvent &event)
    {
        InputSystem &inputSystem = Globals::inputSystem();
        inputSystem.onEvent(event);
        if(mGameState)
        {
            mGameState->keyboardEvent(event);
            mGameState->mouseEvent(event);
        }

        return false;
    }

    // Changes the game state
    void GameManager::changeState(gameStates::IGameState &state)
    {
        mNextState = & state;
        // Call exit state function
        /*if (mGameState)
            mGameState->clear();

        // I verify that the state is a new one.
        if (&state != mGameState)
        {
            mGameState = &state;

            // Call enter state function
            mGameState->init();
        }*/
    }

    GameManager& gameManager()
    {
        static GameManager gameMgr;

        return gameMgr;
    }
}

#pragma once

#include <fstream>
#include <string>

namespace utils
{
    class Logger
    {
    public:
        Logger(const std::string& fileName)
            : mMyFile(fileName.c_str(), std::fstream::app)
        {
            // Log initial info
            mMyFile << "\n\n\n--- NEW GAME ---\n\n\n";
        }

        ~Logger()
        {
            // Close the file.
            mMyFile.close();
        }

        void logInfo(const std::string &info);

    private:
        std::fstream mMyFile;
    };

    Logger& logger();
}

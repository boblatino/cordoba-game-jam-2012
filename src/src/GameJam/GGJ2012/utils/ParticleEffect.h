//////////////////////////////////////////////////////////////////////////
//          A Game Particle effect.
//		   Default particle effect (fire) :
//		   ParticleSize(const irr::core::dimension2d<irr::f32>(20.0f, 10.0f));
//		   StartColour(irr::video::SColor(0, 255, 255, 255));
//		   EndColour(irr::video::SColor(0, 255, 255, 255));
//		   ScaleX(2.0f);
//		   ScaleY(2.0f);
//		   ScaleZ(2.0f);
//		   LifetimeMin(800);
//		   LifetimeMax(2000);
//		   MinParticlesPerSecond(20);
//		   MaxParticlesPerSecond(30);
//		   Sprite("media/sprites/fireball.bmp")
//////////////////////////////////////////////////////////////////////////

#pragma once

#include <cstdint>
#include <dimension2d.h>
#include <SColor.h>
#include <vector3d.h>

#include <game/IGameEntity.h>

namespace irr
{
	typedef char c8;
}

namespace utils
{
    // Data structure to hold a particle effect
    class ParticleEffect 
    {
    public:
        ParticleEffect();
        ~ParticleEffect();

    public:
        uint32_t getMinParticlesPerSecond() const;
        uint32_t getMaxParticlesPerSecond() const;
        uint32_t getLifetimeMin() const;
        uint32_t getLifetimeMax() const;
        uint32_t getID() const;
        const irr::c8 * const getSprite() const;
        uint32_t getTimeForce() const;
        float getScaleX() const;
        float getScaleY() const;
        float getScaleZ() const;
        float getVectorX() const;
        float getVectorY() const;
        float getVectorZ() const;
        irr::core::dimension2d<irr::f32> getParticleSize() const;
        irr::video::SColor getStartColor() const;
        irr::video::SColor getEndColor() const;
        irr::core::vector3df getGravity() const;
        irr::core::vector3df getPosition() const;

        void setMinParticlesPerSecond(const uint32_t minParticlesPerSecond);
        void setMaxParticlesPerSecond(const uint32_t maxParticlesPerSecond);
        void setLifetimeMin(const uint32_t lifetimeMin);
        void setLifetimeMax(const uint32_t lifetimeMax);
        void setID(const uint32_t id);
        void setSprite(irr::c8 * const sprite);
        void setTimeForce(const uint32_t timeForce);
        void setScaleX(const float scaleX);
        void setScaleY(const float scaleY);
        void setScaleZ(const float scaleZ);
        void setVectorX(const float vectorX);
        void setVectorY(const float vectorY);
        void setVectorZ(const float vectorZ);
        void setParticleSize(const irr::core::dimension2d<irr::f32> &size);
        void setStartColor(const irr::video::SColor &color);
        void setEndColor(const irr::video::SColor &cColor);
        void setGravity(const irr::core::vector3df &gravity);
        void setPosition(const irr::core::vector3df &position);

    private:
        irr::core::dimension2d<irr::f32> mParticleSize;
        irr::video::SColor	mStartColor;
        irr::video::SColor	mEndColor;
        irr::core::vector3df mGravity;
        irr::core::vector3df mPosition;

        float mScaleX;
        float mScaleY;
        float mScaleZ;

        float mVectorX;
        float mVectorY;
        float mVectorZ;

        uint32_t mTimeForce;
        uint32_t mMinParticlesPerSecond;
        uint32_t mMaxParticlesPerSecond;

        uint32_t mLifetimeMin;
        uint32_t mLifetimeMax;
        uint32_t mID; 

        irr::c8 *mSprite;
    };
}
#include "ParticleEffect.h"

namespace utils
{
    ParticleEffect::ParticleEffect()
    {
        // default effect is Fire!!
        this->setParticleSize(irr::core::dimension2d<irr::f32>(20.0f, 10.0f));
        this->setStartColor(irr::video::SColor(0, 255, 255, 255));
        this->setEndColor(irr::video::SColor(0, 255, 255, 255));
        this->setScaleX(2.0f);
        this->setScaleY(2.0f);
        this->setScaleZ(2.0f);
        this->setLifetimeMin(800);
        this->setLifetimeMax(2000);
        this->setMinParticlesPerSecond(20);
        this->setMaxParticlesPerSecond(30);
        this->setSprite("media/fireball.bmp");
    }

    ParticleEffect::~ParticleEffect() {}

    // Returns a number that represents the minimum 
    // particles per second for this effect
    uint32_t ParticleEffect::getMinParticlesPerSecond() const
    {
        return mMinParticlesPerSecond;
    }

    // Returns a number that represents the maximum 
    // particles per second for this effect
    uint32_t ParticleEffect::getMaxParticlesPerSecond() const
    {
        return mMinParticlesPerSecond;
    }

    // Returns a number that represents the particle's minimum 
    // lifetime for this effect
    uint32_t ParticleEffect::getLifetimeMin() const
    {
        return mLifetimeMin;
    }

    // Returns a number that represents the particle's maximum 
    // lifetime for this effect
    uint32_t ParticleEffect::getLifetimeMax() const
    {
        return mLifetimeMax;
    }

    // Returns a number that represents the particle's ID
    // used to reference or group effects
    uint32_t ParticleEffect::getID() const
    {
        return mID;
    }

    // Returns a character string that represents the sprite image
    // for this effect
    const irr::c8 * const ParticleEffect::getSprite() const
    {
        return mSprite;
    }

    // Returns a number that represents the time force 
    // for this effect
    uint32_t ParticleEffect::getTimeForce() const
    {
        return mTimeForce;
    } 

    // Returns a float that represents the X axis Scale
    // for this effect, proportions can be changed.
    float ParticleEffect::getScaleX() const
    {
        return mScaleX;
    }

    // Returns a float that represents the Y axis Scale
    // for this effect, proportions can be changed.
    float ParticleEffect::getScaleY() const
    {
        return mScaleY;
    }

    // Returns a float that represents the Z axis Scale
    // for this effect, proportions can be changed.
    float ParticleEffect::getScaleZ() const
    {
        return mScaleZ;
    }

    // Returns a float that represents the X axis Vector
    // for this effect, how the effect is oriented.
    float ParticleEffect::getVectorX() const
    {
        return mVectorX;
    }

    // Returns a float that represents the Y axis Vector
    // for this effect, how the effect is oriented.
    float ParticleEffect::getVectorY() const
    {
        return mVectorY;
    }

    // Returns a float that represents the Y axis Vector
    // for this effect, how the effect is oriented.
    float ParticleEffect::getVectorZ() const
    {
        return mVectorZ;
    }

    // Returns a dimension2D that represents the size,
    // this is the width and height of the effect
    irr::core::dimension2d<irr::f32> ParticleEffect::getParticleSize() const
    {
        return mParticleSize; 
    }

    // Returns an irr::video::SColor that represents the start color of the
    // effect. A color contains the Alpha, Red, Blue, and Green channels
    irr::video::SColor ParticleEffect::getStartColor() const
    {
        return mStartColor;
    }

    // Returns an irr::video::SColor that represents the end color of the
    //! effect. A color contains the Alpha, Red, Blue, and Green channels
    irr::video::SColor ParticleEffect::getEndColor() const
    {
        return mEndColor;
    }

    // Returns a vector3D that represents the Gravity of the effect. This
    // determines how the particles will be affected by gravity
    irr::core::vector3df ParticleEffect::getGravity() const
    {
        return mGravity;
    }

    // Returns a vector3D that represents the world Position of the effect. This
    // determines where the effect will be placed in game
    irr::core::vector3df ParticleEffect::getPosition() const
    {
        return mPosition;
    }

    // Used to set the Minimum particles per second of the effect
    void ParticleEffect::setMinParticlesPerSecond(const uint32_t minParticlesPerSecond)
    {
        mMinParticlesPerSecond = minParticlesPerSecond;
    }

    // Used to set the Maximum particles per second of the effect
    void ParticleEffect::setMaxParticlesPerSecond(const uint32_t maxParticlesPerSecond)
    {
        mMaxParticlesPerSecond = maxParticlesPerSecond;
    }

    // Used to set the particle's Minimum Lifetime of the effect
    void ParticleEffect::setLifetimeMin(const uint32_t lifetimeMin)
    {
        mLifetimeMin = lifetimeMin;
    }

    // Used to set the particle's Maximum Lifetime of the effect
    void ParticleEffect::setLifetimeMax(const uint32_t lifetimeMax)
    {
        mLifetimeMax = lifetimeMax;
    }

    // Used to set the particle's ID for later reference
    void ParticleEffect::setID(const uint32_t id)
    {
        mID = id;
    }

    // Used to set the Sprite image used for the effect
    void ParticleEffect::setSprite(irr::c8 * const sprite)
    {
        mSprite = sprite;
    }

    // Used to set the particle's Timeforce
    void ParticleEffect::setTimeForce(const uint32_t timeForce)
    {
        mTimeForce = timeForce;
    }

    // Used to set the particle's X scale
    void ParticleEffect::setScaleX(const float scaleX)
    {
        mScaleX = scaleX;
    }

    // Used to set the particle's Y scale
    void ParticleEffect::setScaleY(const float scaleY)
    {
        mScaleY = scaleY;
    }
    // Used to set the particle's Z scale
    void ParticleEffect::setScaleZ(const float scaleZ)
    {
        mScaleZ = scaleZ;
    }

    // Used to set the particle's X vector
    void ParticleEffect::setVectorX(const float vectorX)
    {
        mVectorX = vectorX;
    }

    // Used to set the particle's Y vector
    void ParticleEffect::setVectorY(const float vectorY)
    {
        mVectorY = vectorY;
    }

    // Used to set the particle's Z vector
    void ParticleEffect::setVectorZ(const float vectorZ)
    {
        mVectorZ = vectorZ;
    }

    // Used to set the particle's size
    void ParticleEffect::setParticleSize(const irr::core::dimension2d<irr::f32> &size)
    {
        mParticleSize = size;
    }
    // Used to set the particle's start color
    void ParticleEffect::setStartColor(const irr::video::SColor &color)
    {
        mStartColor = color;
    }

    // Used to set the particle's end color
    void ParticleEffect::setEndColor(const irr::video::SColor &color)
    {
        mEndColor = color;
    }

    // Used to set the particle's gravity
    void ParticleEffect::setGravity(const irr::core::vector3df &gravity)
    {
        mGravity = gravity;
    }

    // Used to set the particle's X,Y,Z world position
    void ParticleEffect::setPosition(const irr::core::vector3df &position)
    {
        mPosition = position;
    }
}
#include "Logger.h"

#include <ctime>

namespace utils
{
    // Write info to the log file
    void Logger::logInfo(const std::string &info)
    {
        // Log current time and info
        time_t rawtime;
        time (&rawtime);
        mMyFile << ctime(&rawtime);
        mMyFile << info.c_str();
        mMyFile << "\n\n";
    }

    Logger& logger()
    {
        static Logger loggerInstance("logFile.txt");

        return loggerInstance;
    }
}
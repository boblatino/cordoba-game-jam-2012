#pragma once
#include <vector3d.h>

namespace irr
{
    namespace scene
    {
        class ITriangleSelector;
    }
}

namespace utils
{
    bool areVisible(const irr::core::vector3df &pos1, const irr::core::vector3df &pos2, /*const*/ irr::scene::ITriangleSelector &selector);

    bool isPlayerSeeingPoint(const irr::core::vector3df &pos, /*const*/ irr::scene::ITriangleSelector &selector);
    bool isPointAtLeftOfPlayer(const irr::core::vector3df &pos);
    bool isPointAtRightOfPlayer(const irr::core::vector3df &pos);
    bool isPathObstructed(const irr::core::vector3df &sourcePos, const irr::core::vector3df &targetPos, /*const*/ irr::scene::ITriangleSelector &selector);
}
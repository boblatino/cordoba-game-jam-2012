#include "MathHelper.h"

#include <vector3d.h>

#include <IAnimatedMeshSceneNode.h>
#include <ICameraSceneNode.h>
#include <ISceneCollisionManager.h>
#include <ISceneManager.h>

#include <engine/IrrlichtGlobals.h>

namespace utils
{
    // Return true if the positions are not hampered by the map.
    bool areVisible(const irr::core::vector3df &pos1, const irr::core::vector3df &pos2, /*const*/ irr::scene::ITriangleSelector &selector)
    {
        irr::core::line3d<irr::f32> ray(pos1, pos2);
        irr::core::vector3df collisionPoint;
        irr::core::triangle3df triangle;

        // This is only for compatibility to Irrlicht 1.7.1
        const irr::scene::ISceneNode *node1 = 0;
        const irr::scene::ISceneNode* &node2 = node1;

        return !IrrlichtGlobals::collisionManager().getCollisionPoint(ray, &selector, collisionPoint, triangle, node2);
    }

    bool isPlayerSeeingPoint(const irr::core::vector3df &pos, /*const*/ irr::scene::ITriangleSelector &selector)
    {
        // Get normalized vector from player position to enemy position.
        irr::scene::ISceneManager& sceneMgr = IrrlichtGlobals::sceneManager();
        const irr::core::vector3df playerPos = sceneMgr.getActiveCamera()->getPosition();
        irr::core::vector3df playerToPoint = pos - playerPos;
        playerToPoint.Y = 0.0f;
        playerToPoint.normalize();

        // Get normalized heading vector of the player.
        const irr::core::vector3df playerTarget = sceneMgr.getActiveCamera()->getTarget();
        irr::core::vector3df playerHeading  = playerTarget - sceneMgr.getActiveCamera()->getPosition();
        playerHeading.Y = 0.0f;
        playerHeading.normalize();

        // Calculate cross product vector.
        const irr::f32 dot = playerToPoint.dotProduct(playerHeading);

        // Calculate distance between player and position
        const irr::core::line3df distanceRay(playerPos, pos);
        const irr::f32 distance = distanceRay.getLength();

        //
        // Calculate the cosine of the angle of the following 
        // triangle
        //    ------- halfbase
        //    \      /
        //	   \   /
        //       \/ --> this angle
        //     player position
        //
        const irr::f32 halfbase = 40.0f;

        const irr::f32 hip = sqrt(halfbase * halfbase + distance * distance);

        const irr::f32 cosine = distance / hip;
        const irr::f32 left = cos(acos(cosine) * 2.0f);

        // If dot product of normalized vector is between Z and 1.0
        // then the angle between the two vectors is less than X degrees.
        // Also we control that the two points have not obstructed by the environment.
        return left <= dot && dot <= 1.0f && areVisible(playerPos, pos, selector);
    }

    bool isPointAtLeftOfPlayer(const irr::core::vector3df &pos)
    {
        // Get normalized vector from player position to enemy position.
        irr::scene::ISceneManager& sceneMgr = IrrlichtGlobals::sceneManager();
        const irr::core::vector3df playerPos = sceneMgr.getActiveCamera()->getPosition();
        irr::core::vector3df playerToPoint = pos - playerPos;
        playerToPoint.Y = 0.0f;
        playerToPoint.normalize();

        // Get normalized heading vector of the player.
        const irr::core::vector3df playerTarget = sceneMgr.getActiveCamera()->getTarget();
        irr::core::vector3df playerHeading  = playerTarget - sceneMgr.getActiveCamera()->getPosition();
        playerHeading.Y = 0.0f;
        playerHeading.normalize();

        // Calculate cross product vector.
        const irr::core::vector3df cross = playerToPoint.crossProduct(playerHeading);

        // If Y-coordinate of cross product vector is positive, then the point is at left.
        return cross.Y > 0.0f;
    }

    bool isPointAtRightOfPlayer(const irr::core::vector3df &pos)
    {
        // Get normalized vector from player position to enemy position.
        irr::scene::ISceneManager& sceneMgr = IrrlichtGlobals::sceneManager();
        const irr::core::vector3df playerPos = sceneMgr.getActiveCamera()->getPosition();
        irr::core::vector3df playerToPoint = pos - playerPos;
        playerToPoint.Y = 0.0f;
        playerToPoint.normalize();

        // Get normalized heading vector of the player.
        const irr::core::vector3df playerTarget = sceneMgr.getActiveCamera()->getTarget();
        irr::core::vector3df playerHeading  = playerTarget - sceneMgr.getActiveCamera()->getPosition();
        playerHeading.Y = 0.0f;
        playerHeading.normalize();

        // Calculate cross product vector.
        const irr::core::vector3df cross = playerToPoint.crossProduct(playerHeading);

        // If Y-coordinate of cross product vector is negative, then the point is at right.
        return cross.Y < 0.0f;
    }

    bool isPathObstructed(const irr::core::vector3df &sourcePos, const irr::core::vector3df &targetPos, /*const*/ irr::scene::ITriangleSelector &selector)
    {
        // Line from this Enemy position to target position
        irr::core::line3d<irr::f32> line;
        line.start = sourcePos;

        // I don't understand why, but 100.0f works
        line.start = line.start + (targetPos - sourcePos).normalize()*100.0f;
        line.end = targetPos;

        irr::core::vector3df intersection;
        irr::core::triangle3df tri;

        // This is only for compability to Irrlicht 1.7.1
        const irr::scene::ISceneNode * node1 = 0;
        const irr::scene::ISceneNode * & node2 = node1;

        return IrrlichtGlobals::collisionManager().getCollisionPoint(line, &selector, intersection, tri, node2);
    }
}

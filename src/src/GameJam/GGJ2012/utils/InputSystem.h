#pragma once

#include <IEventReceiver.h>

namespace irr
{
    class SEvent;
}

class InputSystem
{
public:
    enum ButtonID {
        LEFT,
        RIGHT,
        MIDDLE,
        SIZE
    };
public:
    InputSystem()
    {
        memset(mKeyIsDown, 0, irr::KEY_KEY_CODES_COUNT);
    }

    void onEvent(const irr::SEvent& event)
    {
        // Remember whether each key is down or up
        if (event.EventType == irr::EET_KEY_INPUT_EVENT){
            mKeyIsDown[event.KeyInput.Key] = event.KeyInput.PressedDown;
        } else if (event.EventType == irr::EET_MOUSE_INPUT_EVENT) {
            mMouseLastPost[0] = event.MouseInput.X;
            mMouseLastPost[1] = event.MouseInput.Y;

            mMouseButtonPressed[ButtonID::LEFT] = event.MouseInput.isLeftPressed();
            mMouseButtonPressed[ButtonID::RIGHT] = event.MouseInput.isRightPressed();
            mMouseButtonPressed[ButtonID::MIDDLE] = event.MouseInput.isMiddlePressed();
        }
    }

    // This is used to check whether a key is being held down
    bool isKeyDown(const irr::EKEY_CODE keyCode) const
    {
        return mKeyIsDown[keyCode];
    }

    bool isMouseButtonPressed(ButtonID id)
    {
        return mMouseButtonPressed[id];
    }

    irr::s32 getLastMouseX(void)
    {
        return mMouseLastPost[0];
    }
    irr::s32 getLastMouseY(void)
    {
        return mMouseLastPost[1];
    }

private:
    // We use this array to store the current state of each key
    bool mKeyIsDown[irr::KEY_KEY_CODES_COUNT];
    irr::s32 mMouseLastPost[2];
    bool mMouseButtonPressed[ButtonID::SIZE];
};

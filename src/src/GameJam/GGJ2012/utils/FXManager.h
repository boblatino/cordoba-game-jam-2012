#pragma once

#include <EMaterialTypes.h>
#include <cstdint>
#include <irrArray.h>

#include "ParticleEffect.h"

namespace irr
{
	class IrrlichtDevice;
	typedef char c8;

    namespace scene
	{
		class IAnimatedMesh;
		class IBillboardSceneNode;
		class IParticleAffector;
		class IParticleEmitter;
		class IParticleSystemSceneNode;
		class ISceneNode;
	}

	namespace video
	{
		class ITexture;
//		enum E_MATERIAL_TYPE;    // for the new standard
	}
}

namespace utils
{
    // Creates special particle effects, water effects, bump mapping etc.  
    class FXManager 
    {
    public:
        FXManager();
        ~FXManager();

    public:
        bool load(irr::IrrlichtDevice* device);

        bool addParticleEffect(irr::IrrlichtDevice * const device, ParticleEffect * const effect);
        bool addParticleEffect(irr::IrrlichtDevice * const device, ParticleEffect * const effect, const irr::core::vector3df &pos);
        bool addParticleEffect(irr::IrrlichtDevice * const device, ParticleEffect * const effect, irr::scene::IAnimatedMeshSceneNode * const model);

        irr::scene::IParticleSystemSceneNode* createGenericParticleEffect(irr::IrrlichtDevice * const device, const irr::core::vector3df &position,
            const irr::c8 * const sprite, const uint32_t time, const uint32_t size);

        void createWaterEffectNode(irr::IrrlichtDevice * const device, const irr::core::vector3df &position);
        void createWaterEffectNode(irr::IrrlichtDevice * const device, const irr::core::vector3df &position, 
            const irr::core::dimension2d<irr::f32> &size, const irr::c8 * const layer1, const irr::c8 * const layer2);

        irr::scene::ISceneNode* createBumpMapNode(irr::IrrlichtDevice * const device, irr::scene::IAnimatedMesh * const mesh);
        irr::scene::ISceneNode* createBumpMapNode(irr::IrrlichtDevice * const device, irr::scene::IAnimatedMesh * const mesh, 
            const irr::c8 * const colorMapName, const irr::c8 * const heightMapName, const irr::video::E_MATERIAL_TYPE type);

        void createExplosion(irr::IrrlichtDevice * const device, const irr::core::vector3df &pos);
        void loadExplosion(irr::IrrlichtDevice * const device);

    private:
        irr::core::array<irr::video::ITexture*> mSprites;

        irr::scene::IParticleSystemSceneNode *mPS;

        irr::scene::IParticleEmitter *mEmitter;

        irr::scene::IParticleAffector *mAffector;

        irr::scene::IBillboardSceneNode *mExplosion;
    };
}

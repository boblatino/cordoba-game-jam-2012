#include "IGameState.h"

#include <ICursorControl.h>
#include <IGUIEnvironment.h>
#include <IGUIInOutFader.h>

#include <engine/IrrlichtGlobals.h>

namespace gameStates
{
    // Fades the screen In/Out
    void IGameState::fadeInOut()
    {
        // Hide mouse cursor
        IrrlichtGlobals::cursorControl().setVisible(false);

        // Fade in
        mInOutFader = IrrlichtGlobals::guiEnvironment().addInOutFader();
        mInOutFader->setColor(irr::video::SColor(255, 0, 0, 0));
        mInOutFader->fadeIn(1500);
    }
}
#pragma once

#include <gameStates/levels/BaseLevelState.h>

namespace gameStates
{
    class LevelState1 : public BaseLevelState
    {
    public:
        void update();

    protected:
        void initExtensions();
    };
}
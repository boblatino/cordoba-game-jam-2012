#include "LevelState2.h"

#include <ISceneNode.h>

#include <game/GameManager.h>

#include <gameStates/GameStates.h>
#include <gameStates/CreditsState.h>
#include <gameStates/BadEndState.h>

#include <systems/loader/SceneLoader.h>
#include <systems/player/Player.h>

namespace gameStates
{
    // Update, game loop for level, updates, moves and
    // displays all elements used by level
    void LevelState2::update()
    {
        if (playerWon()) 
        {
            game::gameManager().changeState(gameStates::GameStates::creditsState());            
            return;
        }  else if (playerLoose()) {
            game::gameManager().changeState(gameStates::GameStates::badEndState());            
            return;
        }

        BaseLevelState::update();
    }

    void LevelState2::initExtensions()
    {
        Globals::sceneLoader().loadScene(*mWorld, "media/irrScenes/level2.irr");   

        irr::scene::ISceneNode* node = createLiquidBody();
        node->setPosition(irr::core::vector3df(0.0f, -2.0f, 0.0f));

        EnemyInfoVec vec;
        EnemyInfo i;
        i.name = "media/enemy/virus.irrmesh";
        i.minRadius = 20;
        i.maxRadius = 40;
        i.force = 20;
        i.vel = 5;
        i.maxY = 100.0f;
        i.minY = 25.0f;

        vec.push_back(i);
        vec.push_back(i);
        vec.push_back(i);
        vec.push_back(i);
        vec.push_back(i);
        vec.push_back(i);
        vec.push_back(i);
        vec.push_back(i);
        createEnemies(vec);

        mPlayer->configureMovemenetZone(100, 200);
        mPlayer->setPosition(irr::core::vector3df(0,0,100));
    }
}

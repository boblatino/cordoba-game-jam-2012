#pragma once

#include <engine/Globals.h>

#include <gameStates/IGameState.h>

#include <systems/collision/ICollision.h>

#include <irrTypes.h>
#include <SKeyMap.h>
#include <vector3d.h>

#include <cstdint>
#include <vector>
#include <string>

class CollisionSystem;
class irrBulletWorld;
class IRigidBody;
class CameraManager;
class Player;
class HUDManager;
class SpecialEffects;
class Enemy;


namespace irr
{
    namespace gui
    {
        class IGUIInOutFader;
    }

    namespace scene
    {
        class ISceneNode;
    }
}

namespace gameStates
{
    class BaseLevelState : public IGameState, public ICollision
    {
    public:
        BaseLevelState() 
            : IGameState()
            , mWorld(nullptr)
            , mCollisionSystem(nullptr)
            , mInOutFader(nullptr)
            , mCameraMngr(nullptr)
            , mPlayer(nullptr)
            , mHUD(nullptr)
            , mEnemyHealth(0)
            , mEnemyKilled(false)
            , mEnemyHurt(false)
            , mTimeOver(false)
            , mPlayerLoose(false)
        {

        }

    public:
        virtual void init();
        virtual void update();
        virtual void clear();
        virtual void keyboardEvent(const irr::SEvent &event);
        virtual void mouseEvent(const irr::SEvent &event);

        virtual void collides(ICollisionObject* self, ICollisionObject* other);

    protected:

        struct EnemyInfo {
            float minRadius;
            float maxRadius;
            float minY;
            float maxY;
            float vel;
            float force;
            std::string name;
        };

        typedef std::vector<EnemyInfo> EnemyInfoVec;


        virtual void initExtensions() = 0;

        bool playerWon();
        bool playerLoose();
        void draw();
        void updateEnemy(); 
        void registerTarget();
        irr::scene::ISceneNode* createLiquidBody();
        void createCamera();
        void createBulletWorld();
        void initGeneral();
        void handleInput();
        void createEnemies(const EnemyInfoVec &ei);
        void clearEnemies(void);

        void initPlayer();   
        void loadKeyBindings();

        irr::SKeyMap mKeyMap[5];

        irrBulletWorld* mWorld;
        CollisionSystem* mCollisionSystem;

        irr::gui::IGUIInOutFader* mInOutFader;

        Player *mPlayer;
        CameraManager *mCameraMngr;
        HUDManager *mHUD;
        SpecialEffects *mSpecialEffects;
        std::vector<Enemy *> mEnemies;

        uint32_t mEnemyHealth;

        bool mEnemyKilled;
        bool mEnemyHurt;
        bool mTimeOver;
        bool mPlayerLoose;
    };
}

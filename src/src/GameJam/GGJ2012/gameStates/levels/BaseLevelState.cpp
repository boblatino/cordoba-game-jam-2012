#include "BaseLevelState.h"

#include <IBillboardSceneNode.h>
#include <ICameraSceneNode.h>
#include <IGUIInOutFader.h>
#include <IGUIEnvironment.h>
#include <IGUIStaticText.h>
#include <ik_ISoundEngine.h>
#include <IMetaTriangleSelector.h>
#include <IMeshSceneNode.h>
#include <IrrlichtDevice.h>
#include <ISceneManager.h>
#include <ISceneNodeAnimatorCollisionResponse.h>

#include <string>

#include <game/EntitiesID.h>
#include <game/GameManager.h>
#include <game/GamePlayer.h>

#include <engine/IrrlichtGlobals.h>

#include <gameStates/ControlsMenuState.h>
#include <gameStates/CreditsState.h>
#include <gameStates/MenuState.h>

#include <sound/SoundEngine.h>

#include <systems/bulletGenerator/BulletGeneratorSystem.h>
#include <systems/collision/CollisionSystem.h>
#include <systems/loader/SceneLoader.h>
#include <systems/player/Player.h>
#include <systems/hud/HUDManager.h>
#include <systems/camera/CameraManager.h>
#include <systems/specialEffects/SpecialEffects.h>
#include <systems/enemy/Enemy.h>
#include <utils/InputSystem.h>

#include <irrbullet.h>

namespace
{
    irr::core::vector3df getLinearVelocityAccordingCamera()
    {
        irr::core::vector3df cameraRotation = IrrlichtGlobals::sceneManager().getActiveCamera()->getRotation();
        irr::core::matrix4 sphereRotation; 
        sphereRotation.setRotationDegrees(cameraRotation);
        irr::core::vector3df forwardDir(irr::core::vector3df(sphereRotation[8], sphereRotation[9], sphereRotation[10]) * 120);

        return forwardDir;
    }
}

namespace gameStates
{
    // Initialization, loads data required for level
    void BaseLevelState::init() 
    {
        initGeneral();

        createBulletWorld();

        createCamera();

        // Create collision system
        mCollisionSystem = new CollisionSystem(*mWorld);        

        // disable mouse cursor
        irr::gui::ICursorControl &cursorControl = IrrlichtGlobals::cursorControl();
        cursorControl.setVisible(false);

        // Create the camera player and hud
        mPlayer = new Player();
        mPlayer->setVelocity(1);
        mPlayer->setTurretVelocity(1);
        mCameraMngr = new CameraManager(mPlayer);
        mCameraMngr->trackPlayer();

        mHUD = new HUDManager();
        mHUD->setTimeLevel(90.0f); // level seconds
        mHUD->start();

        // create environment
        mSpecialEffects = new SpecialEffects(IrrlichtGlobals::device());
        mSpecialEffects->createGlobules(1200, irr::core::vector3df(500,500,500));

        initExtensions();

        registerTarget();
    }

    // Update, game loop for level, updates, moves and
    // displays all elements used by level
    void BaseLevelState::update()
    {
        // Clear the screen
        irr::video::IVideoDriver& videoDrv = IrrlichtGlobals::videoDriver();
        videoDrv.beginScene(true, true, irr::video::SColor(0, 100, 100, 100));

        // Step the simulation with our delta time
        mWorld->stepSimulation(IrrlichtGlobals::lastTimeFrame() * 1.25f, 120);

        updateEnemy();
        handleInput();

        mCollisionSystem->update();

        draw();

        videoDrv.endScene();

        mCameraMngr->update();
        mSpecialEffects->update();

        mTimeOver = mHUD->update();
        
        // If player loses level, play a sad sound and fade out screen
        // this should be only did if we not already loose (initialized fade in/out)
        if (mTimeOver && !mInOutFader) 
        {            
            sound::SoundEngine::soundEngine().play2D("media/sounds/lose.wav");
            mPlayerLoose = true;
            mInOutFader = IrrlichtGlobals::guiEnvironment().addInOutFader();
            mInOutFader->setColor(irr::video::SColor(255, 255, 0, 0));
            mInOutFader->fadeOut(6000);
        }
    }

    // Clears or destroys resources used by level, clean up.
    void BaseLevelState::clear()
    {       
        delete mCollisionSystem;
        delete mPlayer; 
        delete mCameraMngr; 
        delete mHUD; 
        delete mSpecialEffects; 
        clearEnemies();

        if (mInOutFader) {
            mInOutFader->remove();
        }
        
        // delete mWorld;
        sound::SoundEngine::soundEngine().removeAllSoundSources();

        delete mWorld;

        IrrlichtGlobals::sceneManager().clear();
    }

    // Keyboard event for level (state), main keyboard events
    // passed down by Game manager
    void BaseLevelState::keyboardEvent(const irr::SEvent &event)
    {
        // Keyboard event.
        if (event.EventType == irr::EET_KEY_INPUT_EVENT)
        {
            // A key was pressed down.
            if(event.KeyInput.PressedDown)
            {
                // Pass input down to the specific game state keyboard handler
                if(event.KeyInput.Key == irr::KEY_ESCAPE)
                    game::gameManager().changeState(GameStates::menuState());
            }
        }
    }

    // Mouse event handler passed down by GameManager
    void BaseLevelState::mouseEvent( const irr::SEvent &event)
    {
        // Mouse event.
        if (event.EventType == irr::EET_MOUSE_INPUT_EVENT)
        {
            switch(event.MouseInput.Event) 
            { 
            case irr::EMIE_LMOUSE_PRESSED_DOWN:	
                {
                    // Initialize properties of the new bullet
                    BulletGeneratorSystem::BulletProperties properties;
                    irr::core::vector3df shipPosition = mPlayer->getPosition();
                    //shipPosition.Y += 10.0f;
                    properties.mInitialPosition = shipPosition + mPlayer->getTurretPosition();
                    properties.mMass = 0.2f;
                    properties.mFriction = 2.0f;
                    properties.mLinearVelocity = mPlayer->getLinearVelocityAccordingTurret();
                    properties.mScale = irr::core::vector3df(0.25f, 0.25f, 0.25f); 

                    // Generate a new bullet and register it with collision system
                    IRigidBody* body = Globals::bulletGeneratorSystem().generateBullet(properties, *mWorld);
                    mCollisionSystem->registerCollisionObj(this, body);

                    // Play shot sound
                    sound::SoundEngine::soundEngine().play2D("media/sounds/shot.wav");
                }

                break;
            }
        }
    }

    void BaseLevelState::collides(ICollisionObject* self, ICollisionObject* other)
    {
        if (mEnemyKilled)
            return;

        // If one of the objects that collided is the target. We should finalize current level.
        const bool selfIsTarget = self->getAttributes()->existsAttribute("isTarget") && self->getAttributes()->getAttributeAsBool("isTarget");
        const bool otherIsTarget = other->getAttributes()->existsAttribute("isTarget") && other->getAttributes()->getAttributeAsBool("isTarget");

        if (selfIsTarget || otherIsTarget) 
        {
            mEnemyHurt = true;

            if (mEnemyHealth == 0 && !mInOutFader) {
                mEnemyKilled = true;
                sound::SoundEngine::soundEngine().play2D("media/sounds/Ganar.wav");
                mInOutFader = IrrlichtGlobals::guiEnvironment().addInOutFader();
                mInOutFader->setColor(irr::video::SColor(255, 0, 0, 0));
                mInOutFader->fadeOut(6000);
            }
        }
    }

    bool BaseLevelState::playerWon()
    {
        return mEnemyKilled && mInOutFader->isReady();
    }

    bool BaseLevelState::playerLoose()
    {
        return mPlayerLoose && mInOutFader->isReady();
    }

    void BaseLevelState::draw()
    {
        const bool debugDraw = false;
        mWorld->debugDrawWorld(debugDraw);

        // This call will draw the technical properties of the physics simulation
        // to the GUI environment.
        mWorld->debugDrawProperties(false);

        IrrlichtGlobals::sceneManager().drawAll();	
        IrrlichtGlobals::guiEnvironment().drawAll();
    }

    void BaseLevelState::updateEnemy() 
    {
        for (size_t i = 0, size = mEnemies.size(); i < size; ++i){
            mEnemies[i]->update();
        }
        if (mEnemyHurt && !mInOutFader)
        {
            mEnemyHurt = false;

            irr::gui::IGUIInOutFader* hitInOutFader = IrrlichtGlobals::guiEnvironment().addInOutFader();
            hitInOutFader = IrrlichtGlobals::guiEnvironment().addInOutFader();
            hitInOutFader->setColor(irr::video::SColor(100, 70, 0, 0));
            hitInOutFader->fadeIn(400);

            --mEnemyHealth;
        }
    }

    void BaseLevelState::registerTarget()
    {
        // Register target into collision objects to receive a callback
        // when it collides with a bullet
        for (size_t i = 0; i < mWorld->getNumCollisionObjects(); ++i) 
        {
            ICollisionObject* target = mWorld->getCollisionObjectByIndex(i);
            const bool isTarget = target->getAttributes()->existsAttribute("isTarget") && target->getAttributes()->getAttributeAsBool("isTarget");

            if (isTarget)
            {
                mCollisionSystem->registerCollisionObj(this, target);
                break;
            }
        }
    }

    irr::scene::ISceneNode* BaseLevelState::createLiquidBody()
    {
        // Add liquid body
        ILiquidBody* water = mWorld->addLiquidBody(irr::core::vector3df(-5000,0,5000),irr::core::aabbox3df(0, -10000, 0, 10000, 0, 10000), 500.0f, 200.0f);
        water->setCurrentDirection(irr::core::vector3df(0,0,0));
        water->setInfinite(true);
        water->setInfiniteDepth(true);
        water->setLiquidDensity(0.6f);
        water->setLiquidFriction(0.5f);
        water->setDebugDrawEnabled(false);

        irr::scene::IAnimatedMesh* mesh = IrrlichtGlobals::sceneManager().addHillPlaneMesh( "myHill",
            irr::core::dimension2d<irr::f32>(20,20),
            irr::core::dimension2d<irr::u32>(40,40), 0, 0,
            irr::core::dimension2d<irr::f32>(0,0),
            irr::core::dimension2d<irr::f32>(1000,1000)); 

        irr::scene::ISceneNode* node = IrrlichtGlobals::sceneManager().addWaterSurfaceSceneNode(mesh->getMesh(0), 0.0f, 300.0f, 30.0f);

        node->setMaterialTexture(0, IrrlichtGlobals::videoDriver().getTexture("media/examples/water.jpg"));
        node->setScale(irr::core::vector3df(1000,1,1000));

        node->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);
        node->setMaterialFlag(irr::video::EMF_BACK_FACE_CULLING, false);

        return node;
    }

    void BaseLevelState::createCamera()
    {
        // Load keyboard key bindings
        loadKeyBindings();

        irr::scene::ICameraSceneNode * camera = IrrlichtGlobals::sceneManager().addCameraSceneNodeFPS(nullptr, 50.0f,
            0.15f, -1, mKeyMap, 5, false, 2.5f);

        camera->setPosition(irr::core::vector3df(0.0f, 200.0f, 0.0f));
    }

    void BaseLevelState::createBulletWorld()
    {
        // Create irrBullet World 
        const bool useGImpact = true;
        const bool useDebugDrawer = false;
        mWorld = createIrrBulletWorld(&IrrlichtGlobals::device(), useGImpact, useDebugDrawer);

        // Enable drawing of axis aligned bounding box (AABB) and
        // contact points
        //mWorld->setDebugMode(EPDM_DrawAabb | EPDM_DrawContactPoints);

        mWorld->setGravity(irr::core::vector3df(0.0f, -10.0f, 0.0f));
    }

    void BaseLevelState::initGeneral()
    {
        mEnemyHealth = 5;
        mEnemyKilled = false;
        mEnemyHurt = false;
        mTimeOver = false;
        mPlayerLoose = false; 
        mInOutFader = nullptr;
        mPlayer = nullptr;
        mCameraMngr = nullptr;
        mHUD = nullptr;
        mSpecialEffects =  nullptr;
    }

    void
    BaseLevelState::handleInput()
    {
        InputSystem &inputSystem = Globals::inputSystem();
        const float factor = IrrlichtGlobals::lastTimeFrame() * 100.f;

        // check for mouse positions
        irr::gui::ICursorControl &cursorControl = IrrlichtGlobals::cursorControl();
        irr::core::position2d<irr::f32> relPos = cursorControl.getRelativePosition();
        const float relPosX = relPos.X - 0.5f;
        const float relPosY = relPos.Y - 0.5f;

        // rotate turret
        if (relPosX != 0.0f){
            mPlayer->rotateShip(relPosX * factor * 100.0f);
        }
        if (relPosY != 0.0f){
            mPlayer->rotateTurret(factor * ((relPosY > 0.0f) ? -0.75f : 0.75f));
        }

        if (inputSystem.isKeyDown(irr::KEY_KEY_1)) {
           mPlayer->rotateTurret(IrrlichtGlobals::lastTimeFrame() );
       }
       if (inputSystem.isKeyDown(irr::KEY_KEY_2)) {
           mPlayer->rotateTurret(IrrlichtGlobals::lastTimeFrame());
       }
        // translate vectors
        irr::core::vector3df move(0.0f, 0.0f, 0.0f);
        if (inputSystem.isKeyDown(irr::KEY_KEY_W)) {
            move.Z -= factor;
        }
        if (inputSystem.isKeyDown(irr::KEY_KEY_S)) {
            move.Z += factor;
        }
        if (inputSystem.isKeyDown(irr::KEY_KEY_A)) {
            move.X += factor;
        }
        if (inputSystem.isKeyDown(irr::KEY_KEY_D)) {
            move.X -= factor;
        }
        if (move.X != 0.0f || move.Z != 0.0f) {
            mPlayer->translate(move);
        }

        cursorControl.setPosition(0.5f, 0.5f);
    }

    void
    BaseLevelState::createEnemies(const EnemyInfoVec &ei)
    {
        mEnemies.reserve(ei.size());
        for(size_t i = 0, size = ei.size(); i < size; ++i){
            Enemy *enemy = new Enemy(ei[i].name);
            Enemy::Circumference c1,c2;
            c1.radius = ei[i].minRadius;
            c2.radius = ei[i].maxRadius;
            enemy->setMovementZone(c1,c2);
            mEnemies.push_back(enemy);

            // set a random position
            irr::core::vector3df pos;
            pos.X = (std::rand() % 500) - 250;
            pos.Y = (std::rand() % 100);
            pos.Z = (std::rand() % 100) - 50;
            enemy->setPos(pos);
        }
    }

    void
    BaseLevelState::clearEnemies(void)
    {
        for (size_t i = 0, size = mEnemies.size(); i < size; ++i){
           // delete mEnemies[i];
        }
        mEnemies.clear();
    }

    void BaseLevelState::initPlayer()
    {       
        // Init player.
        Globals::gamePlayer().init(3, 100, 100); 
    }

    void BaseLevelState::loadKeyBindings()
    {
        // Configure keys for movement and jump
        mKeyMap[0].Action = irr::EKA_MOVE_FORWARD;
        mKeyMap[0].KeyCode = irr::KEY_KEY_W;

        mKeyMap[1].Action = irr::EKA_MOVE_BACKWARD;
        mKeyMap[1].KeyCode = irr::KEY_KEY_S;

        mKeyMap[2].Action = irr::EKA_STRAFE_LEFT;
        mKeyMap[2].KeyCode = irr::KEY_KEY_A;

        mKeyMap[3].Action = irr::EKA_STRAFE_RIGHT;
        mKeyMap[3].KeyCode = irr::KEY_KEY_D;  

        mKeyMap[4].Action = irr::EKA_JUMP_UP;
        mKeyMap[4].KeyCode = irr::KEY_SPACE;
    }
}

#include "LevelState1.h"

#include <game/GameManager.h>

#include <gameStates/GameStates.h>
#include <gameStates/CreditsState.h>
#include <gameStates/levels/LevelState2.h>
#include <gameStates/BadEndState.h>

#include <systems/loader/SceneLoader.h>
#include <systems/player/Player.h>

#include <sound/SoundEngine.h>

namespace gameStates
{
    // Update, game loop for level, updates, moves and
    // displays all elements used by level
    void LevelState1::update()
    {
        if (playerWon()) 
        {
            game::gameManager().changeState(gameStates::GameStates::levelState2());            
            return;
        }  else if (playerLoose()) {
            game::gameManager().changeState(gameStates::GameStates::badEndState());            
            return;
        }

        BaseLevelState::update();
    }

    void LevelState1::initExtensions()
    {
        Globals::sceneLoader().loadScene(*mWorld, "media/irrScenes/level1.irr");
        mPlayer->configureMovemenetZone(10, 2000);
        mPlayer->setPosition(irr::core::vector3df(0,0,100));

        /*EnemyInfoVec vec;
        EnemyInfo i;
        i.name = "media/enemy/virus.irrmesh";
        i.minRadius = 40;
        i.maxRadius = 90;
        i.force = 2;
        i.vel = 5;
        i.maxY = 100.0f;
        i.minY = 25.0f;

        for (size_t j = 0; j < 50; ++j)
            vec.push_back(i);
        

        createEnemies(vec);*/

        irr::scene::ISceneNode* node = createLiquidBody();
        node->setPosition(irr::core::vector3df(0.0f, 3.0f, 0.0f));
    }
}

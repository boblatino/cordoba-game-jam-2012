#include "MenuState.h"

#include <cstdint>

#include <ICursorControl.h>
#include <IGUIEnvironment.h>
#include <IGUIImage.h>
#include <IrrlichtDevice.h>
#include <IVideoDriver.h>

#include <game/GameManager.h>

#include <engine/IrrlichtGlobals.h>

#include <gameStates/CreditsState.h>
#include <gameStates/ControlsMenuState.h>
#include <gameStates/GameStates.h>
#include <gameStates/LoadingState.h>

namespace gameStates
{
    void MenuState::init()
    {
        loadMouseCursor();
        
        // Load background image
        mIntroImage = IrrlichtGlobals::guiEnvironment().addImage(irr::core::rect<irr::s32>(0, 0, 1024, 768));
        mIntroImage->setImage(IrrlichtGlobals::videoDriver().getTexture("media/images/frame.jpg"));   
        mIntroImage->setRelativePosition(irr::core::position2di(0, -100));
        
        initMenuButtons();

        // Fade in the main menu.
        fadeInOut();
    }

    void MenuState::update()
    {
        // Clear the screen to black.
        irr::video::IVideoDriver& videoDrv = IrrlichtGlobals::videoDriver();
        videoDrv.beginScene(true, true, irr::video::SColor(0, 0, 0, 0));

        IrrlichtGlobals::guiEnvironment().drawAll();

        mouseOver();
        displayMouse();	

        videoDrv.endScene();	
    }

    void MenuState::clear()
    {
        IGameState::clear();

        mMouseCursor = 0;
        mIntroImage->remove();
        mPlayButtonImage->remove();
        mPlayButtonHighImage->remove();
        mCreditsButtonImage->remove();
        mCreditsButtonHighImage->remove();
        mOptionsButtonImage->remove();
        mOptionsButtonHighImage->remove();
        mExitButtonImage->remove();
        mExitButtonHighImage->remove();
    }

    void MenuState::keyboardEvent(const irr::SEvent &event)
    {
        // Keyboard event.
        if (event.EventType == irr::EET_KEY_INPUT_EVENT)
        {
            // A key was pressed down.
            if(event.KeyInput.PressedDown)
            {
                // Pass input down to the specific game state keyboard handler
                //m_Keys[event.KeyInput.Key] = event.KeyInput.PressedDown;
                if(event.KeyInput.Key == irr::KEY_ESCAPE)
                    exit(0);
            }
        }
    }

    void MenuState::mouseEvent(const irr::SEvent &event)	
    {	// Mouse event.
        if (event.EventType == irr::EET_MOUSE_INPUT_EVENT)
        {
            // Pass input down to the specific game state mouse handler
            if(event.MouseInput.Event == irr::EMIE_LMOUSE_LEFT_UP)
                mouseClicked();
        }
    }

    void MenuState::mouseClicked()
    {
        // Get mouse position
        mMousePos = IrrlichtGlobals::cursorControl().getPosition(); 

        // Check if over Exit button
        if(mExitButtonImage->getRelativePosition().isPointInside(mMousePos)) 
            exit(0);

        // Check if over Controls button
        else if(mOptionsButtonImage->getRelativePosition().isPointInside(mMousePos)) 
            game::gameManager().changeState(GameStates::controlsMenuState());

        // Check if over Credits button
        else if(mCreditsButtonImage->getRelativePosition().isPointInside(mMousePos))
            game::gameManager().changeState(GameStates::creditsState()); 

        // Check if over Play button
        else if(mPlayButtonImage->getRelativePosition().isPointInside(mMousePos))
            game::gameManager().changeState(GameStates::loadingState());
    }

    // Mouse over, check to see if over button to highlight
    void MenuState::mouseOver()
    {
        resetMenuButtons();

        // Get mouse position
        mMousePos = IrrlichtGlobals::cursorControl().getPosition(); 

        // Check if over Play button
        if (mPlayButtonImage->getRelativePosition().isPointInside(mMousePos))
        {
            mPlayButtonHighImage->setVisible(true);
            mPlayButtonImage->setVisible(false);
        } 

        // Check if over Options button
        else if (mOptionsButtonImage->getRelativePosition().isPointInside(mMousePos))
        {
            mOptionsButtonHighImage->setVisible(true);
            mOptionsButtonImage->setVisible(false);
        }

        // Check if over Credits button
        else if (mCreditsButtonImage->getRelativePosition().isPointInside(mMousePos))
        {
            mCreditsButtonHighImage->setVisible(true);
            mCreditsButtonImage->setVisible(false);
        }

        // check if over Exit button
        else if (mExitButtonImage->getRelativePosition().isPointInside(mMousePos)) 
        {
            mExitButtonHighImage->setVisible(true);
            mExitButtonImage->setVisible(false);
        }
    }

    // Reset buttons state
    void MenuState::resetMenuButtons()
    {
        // set highlighted button to off
        mPlayButtonHighImage->setVisible(false);
        mOptionsButtonHighImage->setVisible(false);
        mCreditsButtonHighImage->setVisible(false);
        mExitButtonHighImage->setVisible(false);

        // set normal buttons on
        mPlayButtonImage->setVisible(true);
        mOptionsButtonImage->setVisible(true);
        mCreditsButtonImage->setVisible(true);
        mExitButtonImage->setVisible(true);
    }

    void MenuState::displayMouse()
    {
        // Get mouse position and display it.
        mMousePos = IrrlichtGlobals::cursorControl().getPosition();
        IrrlichtGlobals::videoDriver().draw2DImage(mMouseCursor, irr::core::vector2d<irr::s32>(mMousePos.X, mMousePos.Y),	
            irr::core::rect<irr::s32>(0, 0, mMouseCursor->getSize().Height, mMouseCursor->getSize().Width), 0, irr::video::SColor(255,255,255,255), true);
    }

    void MenuState::loadMouseCursor()
    {
        irr::video::IVideoDriver& videoDrv = IrrlichtGlobals::videoDriver();
        mMouseCursor = videoDrv.getTexture("media/icons/crosshair2.png");
        videoDrv.makeColorKeyTexture(mMouseCursor, irr::core::vector2d<irr::s32>(0, 0));  
        IrrlichtGlobals::cursorControl().setVisible(false);
    }

    void MenuState::initMenuButtons()
    {
        const uint32_t offset = 20.0f;

        // Play buttons
        irr::gui::IGUIEnvironment& gui = IrrlichtGlobals::guiEnvironment();
        irr::video::IVideoDriver& videoDrv = IrrlichtGlobals::videoDriver();
        mPlayButtonHighImage = gui.addImage(irr::core::rect<irr::s32>(256, offset + 448, 256 + 139, offset + 448 + 27));
        mPlayButtonHighImage->setImage(videoDrv.getTexture("media/buttons/play_on.png"));
        mPlayButtonImage = gui.addImage(irr::core::rect<irr::s32>(256, offset + 448, 256 + 139, offset + 448 + 27));
        mPlayButtonImage->setImage(videoDrv.getTexture("media/buttons/play.png"));
        mPlayButtonImage->setUseAlphaChannel(true); 

        // Options buttons
        mOptionsButtonHighImage = gui.addImage(irr::core::rect<irr::s32>(256, offset + 475, 256 + 139, offset + 475 + 27));
        mOptionsButtonHighImage->setImage(videoDrv.getTexture("media/buttons/controls_on.png"));
        mOptionsButtonImage = gui.addImage(irr::core::rect<irr::s32>(256, offset + 475, 256 + 139, offset + 475 + 27));
        mOptionsButtonImage->setImage(videoDrv.getTexture("media/buttons/controls.png"));
        mOptionsButtonImage->setUseAlphaChannel(true); 

        // Credits button
        mCreditsButtonHighImage = gui.addImage(irr::core::rect<irr::s32>(256, offset + 502, 256 + 139, offset + 502 + 27));
        mCreditsButtonHighImage->setImage(videoDrv.getTexture("media/buttons/credits_on.png")); 
        mCreditsButtonImage = gui.addImage(irr::core::rect<irr::s32>(256, offset + 502, 256 + 139, offset + 502 + 27));
        mCreditsButtonImage->setImage(videoDrv.getTexture("media/buttons/credits.png"));
        mCreditsButtonImage->setUseAlphaChannel(true); 

        // Exit button
        mExitButtonHighImage = gui.addImage(irr::core::rect<irr::s32>(256, offset + 529, 256 + 139, offset + 529 + 27));
        mExitButtonHighImage->setImage(videoDrv.getTexture("media/buttons/exit_on.png"));
        mExitButtonImage = gui.addImage(irr::core::rect<irr::s32>(256, offset + 529, 256 + 139, offset + 529 + 27));
        mExitButtonImage->setImage(videoDrv.getTexture("media/buttons/exit.png"));
        mExitButtonImage->setUseAlphaChannel(true); 
    }
}

#pragma once

#include <cstdint>
#include <vector>

#include <gameStates/IGameState.h>

namespace irr
{
	class IrrlichtDevice;
	struct SEvent;

	namespace gui
	{
		class IGUIStaticText;
	}
}

namespace gameStates
{
    // Scrolling game credits screen
    class CreditsState : public IGameState  
    {
    public:
        CreditsState() 
            : IGameState()
            , mLastScrollingTime(0)
            , mScrollingDeltaTime(25)
            , mPosition(0)
        {

        }

        void init();
        void update();
        void clear();

        void keyboardEvent(const irr::SEvent &event);
        void mouseEvent(const irr::SEvent &event);

    private:
        static const uint16_t smCreditSize = 18;
        static const uint16_t smLineSpace = 30;
        static const uint16_t smLeftMargin = 320;
        static const uint16_t smRightMargin = 800;
        static const uint16_t smEndCredits = 800;

        void addCredits();
        void loadCredits();
        void scrollCredits();

        std::vector<wchar_t*> mCredits;
        std::vector<irr::gui::IGUIStaticText*> mCreditItems; // Credits that show in the screen.

        uint32_t mLastScrollingTime;  // Last time that we scroll the credits
        uint32_t mScrollingDeltaTime; // Delta time to wait before scroll the credits
        uint16_t mPosition;          // Credits scroll position.
    };
}
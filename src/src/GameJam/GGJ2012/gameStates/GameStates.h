#pragma once

namespace gameStates
{
    class CreditsState;
    class ControlsMenuState;
    class IntroState;
    class LoadingState;
    class MenuState;
	class PresentationState;
    class LevelState1;
    class LevelState2;
    class BadEndState;

    // Static class to access globally to game states.
    class GameStates
    {
    public:
        static void initAll();
        static void destroyAll();

        static CreditsState& creditsState() { return *mCreditsState; }
        static ControlsMenuState& controlsMenuState() { return *mControlsMenuState; }
        static IntroState& introState() { return *mIntroState; }
        static LoadingState& loadingState() { return *mLoadingState; }
        static MenuState& menuState() { return *mMenuState; }
		static PresentationState& presentationState() { return *mPresentationState; }
        static LevelState1& levelState1() { return *mLevelState1; }
        static LevelState2& levelState2() { return *mLevelState2; }
        static BadEndState& badEndState() { return *mBadEndState; }

    private:
        GameStates();
        ~GameStates();
        GameStates(const GameStates& gameStates);

        GameStates& operator=(const GameStates& gameStates);

        static CreditsState* mCreditsState;
        static ControlsMenuState* mControlsMenuState;
        static IntroState* mIntroState;
        static LoadingState* mLoadingState;
        static MenuState* mMenuState;
		static PresentationState* mPresentationState;
        static LevelState1* mLevelState1;
        static LevelState2* mLevelState2;
        static BadEndState* mBadEndState;
    };
}
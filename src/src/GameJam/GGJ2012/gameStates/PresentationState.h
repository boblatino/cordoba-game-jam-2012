#pragma once

#include <cstdint>

#include <IGUIImage.h>

#include <gameStates/IGameState.h>
#include <systems/video/VideoManager.h>

#include <sound/SoundEngine.h>

namespace irr
{
	struct SEvent;
}

namespace irrklang
{
    class ISound;
}

namespace gameStates
{
    class PresentationState : public IGameState
    {
    public:
        PresentationState() 
            : IGameState()
            , mVideo(nullptr)
            , mSound(nullptr)
			, mExitState(false)
        {
            mVideo = new VideoManager;
        }

        void init();
        void update();
        void clear()
        {
            IGameState::clear();
            
            sound::SoundEngine::soundEngine().removeAllSoundSources();
        }

        void keyboardEvent(const irr::SEvent& /*event*/);
        void mouseEvent(const irr::SEvent& /*event*/) {}

    private:
		VideoManager* mVideo;
        irrklang::ISound* mSound;
		bool mExitState;
    };
}

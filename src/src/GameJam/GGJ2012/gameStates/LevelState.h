#pragma once

#include <engine/Globals.h>
#include <gameStates/IGameState.h>

#include <cstdint>

#include <irrString.h>
#include <irrTypes.h>
#include <SKeyMap.h>
#include <vector3d.h>

class CollisionSystem;
class irrBulletWorld;
class IRigidBody;
class ISoftBody;

namespace gameStates
{
    class LevelState : public IGameState 
    {
    public:
        LevelState() 
            : IGameState()
            , mWorld(nullptr)
            , mTimeStamp(0)
        {

        }

    public:
        void init();
        void update();
        void clear();
        void keyboardEvent(const irr::SEvent &event);
        void mouseEvent(const irr::SEvent &event);

    private:        
        void initPlayer();   
        void loadKeyBindings();

        IRigidBody* const shootCube(const irr::core::vector3df &scale, irr::f32 mass) const;
        IRigidBody* const shootSphere(const irr::core::vector3df &scale, irr::f32 mass) const;

        irr::SKeyMap mKeyMap[5];

        irrBulletWorld* mWorld;

        CollisionSystem* mCollisionSystem;

        uint32_t mTimeStamp;
    };
}
#pragma once

#include <cstdint>

#include <IGUIStaticText.h>

#include <gameStates/IGameState.h>

namespace gameStates
{
    class LoadingState : public IGameState
    {
    public:
        LoadingState() 
            : IGameState() 
            , mTextItem(0)
            , mStateStartTime(0)
            , mDeltaTimeToChangeState(3000)
        {

        }

    public:
        void init();
        void update();
        void clear()
        {
            IGameState::clear();
            mTextItem->remove();
        }

        void keyboardEvent(const irr::SEvent& /*event*/) {}
        void mouseEvent(const irr::SEvent& /*event*/) {}

    private:	
        irr::gui::IGUIStaticText *mTextItem;
        uint32_t mStateStartTime;              // Time of the state starting
        uint32_t mDeltaTimeToChangeState;       // Delta time to wait before state changing.
    };
}

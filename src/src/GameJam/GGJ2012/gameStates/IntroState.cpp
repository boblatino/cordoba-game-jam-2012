#include "IntroState.h"

#include <ICursorControl.h>
#include <IGUIEnvironment.h>
#include <IrrlichtDevice.h>
#include <IVideoDriver.h>

#include <game/GameManager.h>

#include <engine/IrrlichtGlobals.h>

#include <gameStates/CreditsState.h>
#include <gameStates/GameStates.h>
#include <gameStates/IntroState.h>
#include <gameStates/MenuState.h>

namespace gameStates
{
    void IntroState::init()
    {
        // Add Irrlicht logo
        mIntroImage = IrrlichtGlobals::guiEnvironment().addImage(irr::core::rect<irr::s32>(256, 128, 1024, 768));
        mIntroImage->setImage(IrrlichtGlobals::videoDriver().getTexture("media/images/irrlichtlogo.png"));

        fadeInOut();

        // Store the time when the state began.
        mStateStartTime = IrrlichtGlobals::timer().getTime();

        // Hide the mouse cursor.
        IrrlichtGlobals::cursorControl().setVisible(false);
    }

    void IntroState::update()
    {
        // Fill the screen with black and display the image.
        irr::video::IVideoDriver& videoDriver = IrrlichtGlobals::videoDriver();
        videoDriver.beginScene(true, true, irr::video::SColor(0, 0, 0, 0));

        IrrlichtGlobals::guiEnvironment().drawAll();			

        videoDriver.endScene();

        // Check if we must change the current state.
        const uint32_t currentTime = IrrlichtGlobals::timer().getTime();
        if(currentTime - mStateStartTime > mDeltaTimeToChangeState)
            game::gameManager().changeState(GameStates::menuState()); 
    }
}
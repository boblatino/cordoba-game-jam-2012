#pragma once

#include <cstdint>

#include <IGUIImage.h>

#include <gameStates/IGameState.h>

namespace irr
{
	struct SEvent;
}

namespace gameStates
{
    class IntroState : public IGameState
    {
    public:
        IntroState() 
            : IGameState()
            , mIntroImage(0)
            , mStateStartTime(0)
            , mDeltaTimeToChangeState(3000)
        {

        }

        void init();
        void update();
        void clear()
        {
            IGameState::clear();
            mIntroImage->remove();
        }

        void keyboardEvent(const irr::SEvent& /*event*/) {}
        void mouseEvent(const irr::SEvent& /*event*/) {}

    private:
        irr::gui::IGUIImage *mIntroImage;              // Image to display

        uint32_t mStateStartTime;                  // Time of the state starting
        uint32_t mDeltaTimeToChangeState;          // Delta time to wait before change the state.
    };
}

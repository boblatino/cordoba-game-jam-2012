#include "BadEndState.h"

#include <ik_ISound.h>

#include <ICursorControl.h>
#include <IGUIEnvironment.h>
#include <IrrlichtDevice.h>
#include <IVideoDriver.h>

#include <game/GameManager.h>

#include <engine/IrrlichtGlobals.h>

#include <gameStates/GameStates.h>
#include <gameStates/CreditsState.h>

namespace gameStates
{
    void BadEndState::init()
    {
        mVideo->play("media/sounds/line.avi");
        mSound = sound::SoundEngine::soundEngine().play2D("media/sounds/line.wav");

        // Hide the mouse cursor.
        IrrlichtGlobals::cursorControl().setVisible(false);
    }

    void BadEndState::update()
    {
        if(mExitState) {
            game::gameManager().changeState(GameStates::creditsState());

            return;
        }

        // Fill the screen with black and display the image.
        irr::video::IVideoDriver& videoDriver = IrrlichtGlobals::videoDriver();
        videoDriver.beginScene(true, true, irr::video::SColor(0, 0, 0, 0));

        if(!mVideo->update()) {
            mExitState = true;
        }

        videoDriver.endScene();
    }

    void BadEndState::keyboardEvent(const irr::SEvent& event) 
    {
        // Keyboard event.
        if (event.EventType == irr::EET_KEY_INPUT_EVENT)
        {
            // A key was pressed down.
            if(event.KeyInput.PressedDown)
            {
                // Pass input down to the specific game state keyboard handler
                //m_Keys[event.KeyInput.Key] = event.KeyInput.PressedDown;
                if(event.KeyInput.Key == irr::KEY_ESCAPE) {
                    game::gameManager().changeState(GameStates::creditsState());
                }
            }
        }
    }
}
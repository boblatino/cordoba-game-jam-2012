#include "ControlsMenuState.h"

#include <cstdint>

#include <ICursorControl.h>
#include <IGUIEnvironment.h>
#include <IGUIImage.h>
#include <IGUIStaticText.h>
#include <irrString.h>	
#include <IVideoDriver.h>

#include <game/GameManager.h>

#include <engine/IrrlichtGlobals.h>

#include <gameStates/GameStates.h>
#include <gameStates/MenuState.h>

namespace gameStates
{
    void ControlsMenuState::init()
    {	
        mMousePos = irr::core::vector2d<int>(0,0);
        loadMouseCursor();

        // Load controls text
        irr::gui::IGUIEnvironment& gui = IrrlichtGlobals::guiEnvironment();
        mControlsText = gui.addStaticText(L"", irr::core::rect<int>(250, 50, 1000, 500), false);
        mControlsText->setOverrideColor(irr::video::SColor(128, 200, 200, 200));
        mControlsText->setOverrideFont(gui.getFont("media/fonts/font_gothic18.bmp"));

        // Set controls.
        irr::core::stringw str = "";
        str += "Move Forward = W \n";
        str += "Move Backward = S \n";
        str += "Strafe Left = A \n";
        str += "Strafe Right = D \n";
        str += "Aim Weapon = Mouse \n";
        str += "Shoot = Left Mouse Button \n";
        str += "Exit Level = ESC \n";
        mControlsText->setText(str.c_str());

        // Exit button
        irr::video::IVideoDriver& videoDrv = IrrlichtGlobals::videoDriver();
        mExitButtonHighImage = gui.addImage(irr::core::rect<int>(256, 529, 256 + 139, 529 + 27));
        mExitButtonHighImage->setImage(videoDrv.getTexture("media/buttons/exit_on.png"));
        mExitButtonImage = gui.addImage(irr::core::rect<irr::s32>(256, 529, 256 + 139, 529 + 27));
        mExitButtonImage->setImage(videoDrv.getTexture("media/buttons/exit.png"));
        mExitButtonImage->setUseAlphaChannel(true); 

        fadeInOut();
    }

    void ControlsMenuState::update()
    {
        // Clear the screen to black.
        irr::video::IVideoDriver& videoDrv = IrrlichtGlobals::videoDriver();
        videoDrv.beginScene(true, true, irr::video::SColor(0, 0, 0, 0));

        IrrlichtGlobals::guiEnvironment().drawAll();

        mouseOver();
        displayMouse();

        videoDrv.endScene();	
    }

    void ControlsMenuState::clear()
    {
        mMouseCursor = 0;
        mControlsText->remove();
        mExitButtonImage->remove();
        mExitButtonHighImage->remove();
    }

    // Keyboard event handler passed down by GameManager
    void ControlsMenuState::keyboardEvent(const irr::SEvent &event)
    {
        // Keyboard event.
        if (event.EventType == irr::EET_KEY_INPUT_EVENT)
        {
            // A key was pressed down.
            if(event.KeyInput.PressedDown)
            {
                if(event.KeyInput.Key == irr::KEY_ESCAPE)
                    game::gameManager().changeState(GameStates::menuState()); 
            }
        }
    }

    // Mouse event handler passed down by GameManager
    void ControlsMenuState::mouseEvent(const irr::SEvent &event)
    {
        // Mouse event.
        if (event.EventType == irr::EET_MOUSE_INPUT_EVENT)
        {
            if(event.MouseInput.Event == irr::EMIE_LMOUSE_LEFT_UP)
                mouseClicked();
        }
    }

    // Mouse clicked, check to see if over button
    void ControlsMenuState::mouseClicked()
    {
        // get mouse position2d<s32>
        mMousePos = IrrlichtGlobals::cursorControl().getPosition(); 

        // check if over Exit button
        if (mExitButtonImage->getRelativePosition().isPointInside(mMousePos)) 
            game::gameManager().changeState(GameStates::menuState()); 
    }

    // Mouse over, check to see if over button to highlight
    void ControlsMenuState::mouseOver()
    {
        resetButtons();

        // Get mouse position
        mMousePos = IrrlichtGlobals::cursorControl().getPosition(); 

        // Check if over Exit button
        if (mExitButtonImage->getRelativePosition().isPointInside(mMousePos)) 
        {
            mExitButtonHighImage->setVisible(true);
            mExitButtonImage->setVisible(false);
        }
    }

    // Display mouse cursor 
    void ControlsMenuState::displayMouse()
    {
        // Crosshair X and Y positions
        const uint16_t crosshairX = mMousePos.X - (mMouseCursor->getSize().Width / 2);
        const uint16_t crosshairY = mMousePos.Y - (mMouseCursor->getSize().Height / 2);

        mMousePos = IrrlichtGlobals::cursorControl().getPosition();
        IrrlichtGlobals::videoDriver().draw2DImage(mMouseCursor, irr::core::vector2d<irr::s32>(crosshairX, crosshairY),	
            irr::core::rect<int>(0, 0, mMouseCursor->getSize().Height, mMouseCursor->getSize().Width), 0, irr::video::SColor(255, 255, 255, 255),
            true);
    }

    void ControlsMenuState::loadMouseCursor()
    {
        irr::video::IVideoDriver& videoDrv = IrrlichtGlobals::videoDriver();
        mMouseCursor = videoDrv.getTexture("media/icons/crosshair2.png");
        videoDrv.makeColorKeyTexture(mMouseCursor, irr::core::vector2d<int>(0, 0)); 
        IrrlichtGlobals::cursorControl().setVisible(false);
    }
}

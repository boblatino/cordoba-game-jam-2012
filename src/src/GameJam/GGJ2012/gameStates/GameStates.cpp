#include "GameStates.h"

#include <gameStates/CreditsState.h>
#include <gameStates/ControlsMenuState.h>
#include <gameStates/IntroState.h>
#include <gameStates/LoadingState.h>
#include <gameStates/MenuState.h>
#include <gameStates/PresentationState.h>
#include <gameStates/levels/LevelState1.h>
#include <gameStates/levels/LevelState2.h>
#include <gameStates/BadEndState.h>

namespace gameStates
{
    CreditsState* GameStates::mCreditsState = nullptr;
    ControlsMenuState* GameStates::mControlsMenuState = nullptr;
    IntroState* GameStates::mIntroState = nullptr;
    LoadingState* GameStates::mLoadingState = nullptr;
    MenuState* GameStates::mMenuState = nullptr;
	PresentationState* GameStates::mPresentationState = nullptr;
    LevelState1* GameStates::mLevelState1 = nullptr;
    LevelState2* GameStates::mLevelState2 = nullptr;
    BadEndState* GameStates::mBadEndState = nullptr;

    void GameStates::initAll()
    {
        mCreditsState = new CreditsState();
        mControlsMenuState = new ControlsMenuState();
        mIntroState = new IntroState();
        mLoadingState = new LoadingState();
        mMenuState = new MenuState();
		mPresentationState = new PresentationState();
        mLevelState1 = new LevelState1();
        mLevelState2 = new LevelState2();
        mBadEndState = new BadEndState();
    }

    void GameStates::destroyAll()
    {
        delete mCreditsState;
        delete mControlsMenuState;
        delete mIntroState;
        delete mLoadingState;
        delete mMenuState;
        delete mPresentationState;
        delete mLevelState1;
        delete mLevelState2;
        delete mBadEndState;
    }
}
